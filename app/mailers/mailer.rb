class Mailer < ApplicationMailer
	default from: 'notifications@example.com'
 
  def welcome_email(user)
    @user = user
    @url  = 'http://example.com/login'
    mail(to: @user.email, subject: 'Welcome to My Awesome Site')
  end

  def new_user_invite(user,ruta)
  	@user = user
  	@ruta = ruta
  	mail(to: @user.email, subject: 'Welcome')  	
  end

  def instructions(invitation)
    @token    = invitation.invitation_token
    @sender   = invitation.sent_by_id
    @app_name = app_name

    mail(
      subject: t('.subject'),
      from:    invitation.email, #sender_email,
      to:      invitation.email,
    )
  end

  protected

  def sender_email
    @sender.email
  end

  def app_name
    Rails.application.class.parent_name
      .underscore
      .humanize
      .split
      .map(&:capitalize)
      .join(' ')
  end
end
