json.extract! users_organizations_role, :id, :users_organization, :role_id, :created_at, :updated_at
json.url users_organizations_role_url(users_organizations_role, format: :json)
