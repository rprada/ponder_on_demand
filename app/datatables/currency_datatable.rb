class CurrencyDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :link_to, :h, :mailto, :edit_currency_path, :other_method

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      name:       { source: "Currency.name",       cond: :like, searchable: true, orderable: true },
      sort_name:  { source: "Currency.name",       cond: :like, searchable: true, orderable: true },
      symbol:     { source: "Currency.symbol",     cond: :like, searchable: true, orderable: true },
      status:     { source: "StatusEntity.name",   cond: :like, searchable: true, orderable: true },
    }
  end
  def data
    records.map do |record|
      {
        name:       record.name,
        sort_name:  record.sort_name,
        symbol:     record.symbol,
        status:     record.status_name,
        actions:    link_to("<span style='cursor:pointer' class='icon-document-fill icon-size-medium highlight-color-gray-text'></span>".html_safe, edit_currency_path(id: record.id))+ " " +
                    link_to("<span style='cursor:pointer' class='icon-cross icon-size-medium highlight-color-gray-text'></span>".html_safe, "", class: "deleteConfirm", data: { id: record.id } )
      }
    end
  end

  private

  def get_raw_records
    Currency.select("currencies.id, currencies.name, currencies.sort_name,
                     currencies.symbol, status_entities.name as status_name").
    joins( :status_entity )
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end