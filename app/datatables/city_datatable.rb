class CityDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :link_to, :h, :mailto, :edit_city_path, :other_method

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      name:     { source: "City.name",           cond: :like, searchable: true, orderable: true },
      state:    { source: "State.name",          cond: :like, searchable: true, orderable: true },
      county:   { source: "County.name",         cond: :like, searchable: true, orderable: true },
      status:   { source: "StatusEntity.name",   cond: :like, searchable: true, orderable: true },
    }
  end
  def data
    records.map do |record|
      {
        name:      record.name,
        state:     record.state_name,
        county:    record.county_name,
        status:    record.status,
        actions:   link_to("<span style='cursor:pointer' class='icon-document-fill icon-size-medium highlight-color-gray-text'></span>".html_safe, edit_city_path(id: record.id))+ " " +
                   link_to("<span style='cursor:pointer' class='icon-cross icon-size-medium highlight-color-gray-text'></span>".html_safe, "", class: "deleteConfirm", data: { id: record.id } )
      }
    end
  end

  private

  def get_raw_records
    City.select("cities.id, cities.name, states.name AS state_name, 
                 status_entities.name as status, counties.name AS county_name").
    joins( :state, :county, :status_entity )
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end