class UsersOrganizationDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :link_to, :h, :mailto, :edit_users_organization_path, :users_organization_path, :other_method

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      full_name: { source: "User.full_name",    cond: :like, searchable: true, orderable: true },
      email:     { source: "User.email",        cond: :like, searchable: true, orderable: true },
      role:      { source: "Role.name" ,        cond: :like, searchable: true, orderable: true },
      status:    { source: "StatusEntity.name", cond: :like, searchable: true, orderable: true  },
    }
  end

  def data
    records.map do |record|
      {
        full_name: record.full_name,
        email:     record.email,
        role:      record.role,
        status:    record.status,
        actions:   link_to("<span style='cursor:pointer' class='icon-document-fill icon-size-medium highlight-color-gray-text'></span>".html_safe, edit_users_organization_path(id: record.id)),
      }
    end
  end

  private

  def get_raw_records
    UsersOrganization
    .select("users_organizations.id, users.full_name, users.email, 
             organizations.name as organization_name, status_entities.name as status,
             roles.name as role")
    .joins(:user, :organization, :status_entity, [users_organizations_role: :role])
    .where(organization_id: UsersOrganization.current.organization_id)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
