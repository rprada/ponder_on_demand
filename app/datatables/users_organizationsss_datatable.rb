class UsersOrganizationDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :link_to, :fa_icon, :button_to
  def searchable_columns
    @searchable_columns ||= %w( User.full_name User.email Organization.name StatusEntity.name User.id)
  end
  def sortable_columns
    @sortable_columns ||= %w( User.full_name User.email Organization.name StatusEntity.name User.id)
  end
  private
  def data
    records.map do |record|
      [
        record.full_name,
        record.email,
        record.role,
        record.status,
        "<span style='cursor:pointer' data-id='#{record.id}' class='edit icon-document-fill icon-size-medium'></span>" + " " +
        "<span style='cursor:pointer' data-id='#{record.id}' class='deleteConfirm icon-cross icon-size-medium'></span>"
      ]
    end
  end
  def get_raw_records
    UsersOrganization
    .select("users_organizations.id, users.full_name, users.email, 
             organizations.name as organization_name, status_entities.name as status,
             roles.name as role")
    .joins(:user, :organization, :status_entity, [users_organizations_role: :role])
    .where(organization_id: UsersOrganization.current.organization_id)
  end
  def conditions
    conditions_search
  end
  def conditions_search
    if params[:sSearch].present?
      [ " users.full_name                    LIKE :search OR
          users.email                        LIKE :search OR
          organizations.organization_name    LIKE :search OR
          status_entities.name               LIKE :search  ",
          search: "%#{params[:sSearch]}%" ]
    end
  end
end