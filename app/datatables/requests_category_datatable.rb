class RequestsCategoryDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :link_to, :h, :mailto, :edit_requests_category_path, :fa_icon
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      name:      { source: "RequestsCategory.name", cond: :like, searchable: true, orderable: true },
      status:    { source: "StatusEntity.name",     cond: :like, searchable: true, orderable: true },
      scorecard: {  },

    }
  end
  def data
    records.map do |record|
      {
        name:      record.name,
        status:    record.status_name,
        scorecard: record.cont > 0 ? link_to(fa_icon('check-square-o' , text: "Active"), "/#{params[:locale]}/scorecards/#{record.scorecard_id}/edit", class: 'btn btn-success dropdown-toggle btn-xs') : link_to(fa_icon('clock-o' , text: "Inactive"), "/#{params[:locale]}/scorecards/#{record.scorecard_id}/edit", class: 'btn btn-default dropdown-toggle btn-xs'),
        actions:   link_to("<span style='cursor:pointer' class='icon-document-fill icon-size-medium highlight-color-gray-text'></span>".html_safe, edit_requests_category_path(id: record.id))+ " " +
                   link_to("<span style='cursor:pointer' class='icon-cross icon-size-medium highlight-color-gray-text'></span>".html_safe, "", class: "deleteConfirm", data: { id: record.id } )
      }
    end
  end

  private
  def get_raw_records
    RequestsCategory.
    select("COUNT(questions.id) AS cont, requests_categories.id, scorecards.status_entity_id AS scorecard_status,
            requests_categories.name, requests_categories.scorecard, status_entities.name AS status_name, scorecards.id AS scorecard_id").
    joins("LEFT JOIN status_entities
                ON status_entities.id = requests_categories.status_entity_id AND status_entities.`deleted_at` IS NULL
           LEFT JOIN scorecards 
                ON scorecards.requests_category_id = requests_categories.id AND scorecards.`deleted_at` IS NULL
           LEFT JOIN questions
                ON questions.scorecard_id = scorecards.id AND questions.`deleted_at` IS NULL").
    group("requests_categories.id")
  end
  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them
  # def filter_records(records)
  # end
  # def sort_records(records)
  # end
  # def paginate_records(records)
  # end
  # ==== Insert 'presenter'-like methods below if necessary
end