class UserDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :link_to, :h, :mailto, :edit_user_path, :other_method

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      full_name:    { source: "User.full_name",     cond: :like, searchable: true, orderable: true },
      email:        { source: "User.email",         cond: :like, searchable: true, orderable: true },
      status:       { source: "StatusEntity.name",  cond: :like, searchable: true, orderable: true },
    }
  end
  def data
    records.map do |record|
      {
        full_name:    record.full_name,
        email:        record.email,
        status:       record.status,
        actions:      link_to("<span style='cursor:pointer' class='icon-document-fill icon-size-medium highlight-color-gray-text'></span>".html_safe, edit_user_path(id: record.id))+ " " +
                      link_to("<span style='cursor:pointer' class='icon-cross icon-size-medium highlight-color-gray-text'></span>".html_safe, "", class: "deleteConfirm", data: { id: record.id } )
      }
    end
  end

  private

  def get_raw_records
    User.select("users.id, users.full_name, users.email, status_entities.name AS status").
    joins(:status_entity)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end