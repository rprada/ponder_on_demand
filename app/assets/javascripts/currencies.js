var i18n = $('#body').attr('data-locale'); 
/* Registration Form */
$('#formCurrency')
.formValidation({
    framework: "bootstrap",
    icon: {
      valid: 'icon icon-check',
      invalid: 'icon icon-cross',
      validating: 'icon icon-refresh'
    },
    excluded: ':disabled', // <=== Add the excluded opti
    fields: {
        'currency[name]': {
            validators: {
                notEmpty: {
                  message: i18n == "en" ? 'The currency name is required and cannot be empty' : 'Nombre es obligatorio y no puede estar vacío'
                },
            }
        },
        'currency[sort_name]': {
            validators: {
                notEmpty: {
                    message: i18n == "en" ? 'The sort name is required and cannot be empty' : 'Nombre Ordenado es obligatorio y no puede estar vacío'
                },
                stringLength: {
                    max: 6,
                    message: i18n == "en" ? 'The sort name must be less than 6 characters long' : 'Nombre Ordenado debe ser menor de 6 carácteres'
                },
            }
        },
        'currency[symbol]': {
            validators: {
                notEmpty: {
                    message: i18n == "en" ? 'The symbol is required and cannot be empty' : 'Símbolo es obligatorio y no puede estar vacío'
                }
            }
        },
        'currency[country_id]': {
            validators: {
                notEmpty: {
                  message: i18n == "en" ? 'Please select country' : 'Por favor seleccione país'
                },
            }
        }
    }
});

$('#currency_country_ids').chosen({
  placeholder_text: i18n == "en" ? 'Select a Country' : 'Seleccione País',
  width: "100%",
  //allow_single_deselect: false,
  //max_selected_options: 5,
});
$('#small-modal').appendTo("body") 
$(document).off('click', "#saveConfirm").on('click', "#saveConfirm", function (e) {
  if( $("#formCurrency").data('formValidation').validate().isValid() ) {
  e.preventDefault();
    $('#small-modal').modal('show');
    var message = $('#modal_save').val();
    $('.modal-body').empty();
    $('.modal-body').append(message);
    var $submit = $('#small-modal').find('.btn-primary');
    $submit.attr('onclick','$("#formCurrency").trigger("submit.rails")');
  }
});
$(document).off('click', "#updateConfirm").on('click', "#updateConfirm", function (e) {
  if( $("#formCurrency").data('formValidation').validate().isValid() ) {
    e.preventDefault();
    $('#small-modal').modal('show');
    var message = $('#modal_update').val();
    $('.modal-body').empty();
    $('.modal-body').append(message);
    var $submit = $('#small-modal').find('.btn-primary');
    $submit.attr('onclick','$("#formCurrency").trigger("submit.rails")');
  }
});
$(document).off('click', ".deleteConfirm").on('click', ".deleteConfirm", function (e) {
  e.preventDefault();
  $('#small-modal').modal('show');
  var message = $('#modal_delete').val();
  $('.modal-body').empty();
  $('.modal-body').append(message);
  var $submit = $('#small-modal').find('.btn-primary');
  $submit.attr('data-method','delete');
  $submit.attr('href', '/en/currencies/' + $(this).data('id'));
});
$(document).off('click', "#cancelConfirm").on('click', "#cancelConfirm", function (e) {
    e.preventDefault();
    $('#small-modal').modal('show');
    var message = $('#modal_cancel').val();
    $('.modal-body').empty();
    $('.modal-body').append(message);
    var $submit = $('#small-modal').find('.btn-primary');
    $submit.removeAttr('onclick');
    $submit.removeAttr('data-remote');
    $submit.attr('href', '/' + i18n + '/currencies/');
});

if( $("#action").size() > 0)
{   if ($("#action").val() == "edit")
    {
        $('#button_new').css('display','none');
        $('#buttons').css('display','block');
        $("#formCurrency :input").removeAttr("disabled"); 
        $('.chosen-select').trigger('chosen:updated');       
    }
}
var elem = document.querySelector('.js-switch');
var init = new Switchery(elem);
$('#new').on('click', function(){   
  $('#button_new').css('display','none');
  $('#buttons').css('display','block');
  $("#formCurrency :input").removeAttr("disabled");
  $('.chosen-select').trigger('chosen:updated');
  init.enable();
});

$("#currencies").DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": $('#currencies').data('source'),
    "pagingType": 'simple_numbers',
    /*autoWidth: false,*/
    "bLengthChange": false,
    "iDisplayLength": 8, // Number of rows to display on a single page when using pagination.
    //"order": [[ 0, "desc" ]],
    /*columnDefs: [
       { orderable: false, targets: -1 }
    ]*/
    "columns": [
      {"data": "name"},
      {"data": "sort_name"},
      {"data": "symbol"},
      {"data": "status"},
      {"data": "actions", orderable: false }
    ]
});

function changeCurrency(id){
  $('#small-modal').attr('data-id',id).modal('show');
  var message = $('#modal_change').val();
  $('.modal-body').empty();
  $('.modal-body').append(message);
  var $submit = $('#small-modal').find('.btn-primary');
  $submit.attr('id','change');
}
$(document).off('click', "#change").on('click', "#change", function () { 
  $("#small-modal").modal("hide");         
  var id = $('#small-modal').attr('data-id');
  Turbolinks.visit( '/' + i18n + '/currencies/' + id + '/edit' );
});