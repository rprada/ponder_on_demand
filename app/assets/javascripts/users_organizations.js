var i18n = $('#body').attr('data-locale');
if ( $("#action").val() == "edit") 
 {  $('#form_users_organization')
    .formValidation({
        framework: "bootstrap",
        icon: {
          valid: 'icon icon-check',
          invalid: 'icon icon-cross',
          validating: 'icon icon-refresh'
        },
        excluded: ':disabled', // <=== Add the excluded opti
        fields: {
          'users_organization[users_organization_id]':{
            validators: {
              /*notEmpty: {
                  message: i18n == "en" ? 'Please select report to' : 'Por favor seleccione reportar a'
              },*/
              callback: {
                callback: function(value, validator, $field) {
                  var value = $field.val();    
                  var user_id =  $('#form_users_organization').find('[name="users_organization[id]"]').val(); 
                  var list = $('#form_users_organization').find('[name="users_organization[managed_by]"]').val();                  
                  if (list.split(',').length > 0 && value != "")
                  { if ( jQuery.inArray(value, list.split(',')) != -1 )
                    { return{ 
                        valid: false,                                    
                        message: i18n == "en" ? 'No user can be in report to and manage to at the same time' : 'Ningún usuario puede estar en reportar a y administra a al mismo tiempo'
                      }
                    }
                    if (value == user_id)
                    { return{
                        valid: false,
                        message: i18n == "en" ? 'The user have to be different than himself' : 'El usuario tiene que ser diferente a si mismo'
                      }
                    }
                  }                                
                  return true;
                }
              }
            }  
          },
          'users_organization[managed_by]':{
            validators: {
              callback: {
                callback: function(value, validator, $field) {
                  var value = $field.val(),                  
                  report_to = $('#form_users_organization').find('[name="users_organization[users_organization_id]"]').val(),
                  user_id   = $('#form_users_organization').find('[name="users_organization[id]"]').val();
                  if (value.split(',').length > 0 /*&& report_to != ""*/)
                  { if ( jQuery.inArray(report_to, value.split(',')) !== -1 )
                    {
                      return{
                        valid: false,
                        message: i18n == "en" ? 'No user can be in report to and manage to at the same time' : 'Ningún usuario puede estar en reportar a y administra a al mismo tiempo'
                      }
                    }
                    if (jQuery.inArray(user_id, value.split(',')) !== -1 )
                    { 
                      return{
                        valid: false,
                        message: i18n == "en" ? 'The user have to be different than himself' : 'El usuario tiene que ser diferente a si mismo'
                      }
                    }
                  }
                  return true;
                }
              },
            }  
          }
        
        }
    })      
    .on('change', '[name="users_organization[users_organization_id]"]', function(e) {
        $('#form_users_organization').formValidation('revalidateField', 'users_organization[users_organization_id]')
                                     .formValidation('revalidateField', 'users_organization[managed_by]');
    })               
    .on('change', '[name="users_organization[managed_by]"]', function(e) {
        $('#form_users_organization').formValidation('revalidateField', 'users_organization[managed_by]')
                                     .formValidation('revalidateField', 'users_organization[users_organization_id]');
    });
}

var elem = document.querySelector('.js-switch');
var init = new Switchery(elem);

if ($('#action').val() == 'index') {
  $('#user_organization_status_entity_id').trigger('click');
  init.disable();
};

$("#users_organizations-data").DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": $('#users_organizations-data').data('source'),
    "pagingType": 'simple_numbers',
    /*autoWidth: false,*/
    "bLengthChange": false,
    "iDisplayLength": 8, // Number of rows to display on a single page when using pagination.
    //"order": [[ 0, "desc" ]],
    /*columnDefs: [
       { orderable: false, targets: -1 }
    ]*/
    "columns": [
      {"data": "full_name"},
      {"data": "email"},
      {"data": "role"},
      {"data": "status"},
      {"data": "actions", orderable: false }
    ]
});

$(document).on('click','#updateConfirm', function(a){
  //if ($('#form_user_organization').data("formValidation").validate().isValid()) {
    a.preventDefault();
    $('#small-modal').find('.btn-primary').attr('id','update');
    var text =  $('#modal_update').val();
    $('.modal-body').empty();
    $('.modal-body').append(text);
    $('#small-modal').modal('show');
  //}
});

$(document).off('click', "#update").on('click', "#update", function () {
  $("#form_users_organization").trigger('submit.rails');
  $('#small-modal').modal('hide');
});

$(document).on('click','#cancelConfirm', function(a){
  a.preventDefault();
  $('#small-modal').find('.btn-primary').attr('id','cancel');
  var text =  $('#modal_cancel').val();
  $('.modal-body').empty();
  $('.modal-body').append(text);
  $('#small-modal').modal('show');
});

$(document).on('click','#cancel', function(){
  if ($('#users_organization_id').length) {
    Turbolinks.visit("<%#= users_organizations_path %>");
  } else {
    $('#small-modal').modal('hide');
    disable_form();
  }
});

// -------------------- Others ----------------------------
$('.chosen-select').chosen({
  placeholder_text: 'Select a Role',
  width: "100%",
});

$('#users_organization_users_organization_id').chosen({
  allow_single_deselect: true,
  placeholder_text: 'Select a report to',
  width: "100%",
});

/* Input Tags - Color-Coded */
var users = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: '/users_organizations/1/data_json'
});
users.clearPrefetchCache();
users.initialize();
var elt = $('.input-tags-2');
elt.tagsinput({
    tagClass: 'badge highlight-color-blue',
    itemValue: 'value',
    itemText: 'text',
    typeaheadjs: {
        name: 'users',
        displayKey: 'text',
        source: users.ttAdapter()
    }
});

if ( $('.tags').size() > 0 ) 
{ var data = $('.tags').val();
  var obj = $.parseJSON(data);
  $.each(obj, function(i, item) {
    $('.input-tags-2').tagsinput('add', { "value": item.value , "text": item.text });
  });
}

function disable_form (){
  $('#form_user_organization')[0].reset();
  $('.form-group').removeClass('has-error has-success');
  $('.form-control-feedback').removeClass('icon icon-check icon-cross icon-refresh');
  $('#form_user_organization :input').attr('disabled','disabled');
  $('#form_user_organization .help-block').css('display','none');
  $('#button_new').css("display", "block");
  $('#buttons').css("display", "none");
  init.disable();
};

if ($('#action').val() == 'edit') {
  $('#buttons').css("display", "block");
  $('.chosen-select').removeAttr('disabled');
  $('#users_organization_users_organization_id').removeAttr('disabled');
  $('#users_organization_job_title').removeAttr('disabled');
  $('.input-tags-2').removeAttr('disabled');
  $('.chosen-select').trigger('chosen:updated');
  init.enable();
};