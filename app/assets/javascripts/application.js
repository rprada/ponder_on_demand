// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require turbolinks
//= require toastr
//= require bootstrap.min
//= require icheck.min
//= require formValidation.min
//= require formvalidation/framework/bootstrap.min
//= require formvalidation/language/es_ES
//= require rails.validations
//= require jquery.bootstrap.wizard.min
//= require modals
//= require switchery.min
//= require maskedinput
//= require chosen-jquery
//= require bootstrap-typeahead-rails
//= require bootstrap-tagsinput
//= require cocoon

$(document).ready(function () {
  // Search form.
  $('#invitations_search input').keyup(function () {
    $.get($('#invitations_search').attr('action'), 
      $('#invitations_search').serialize(), null, 'script');
    return false;
  });
})