$(document).ready(function () {
    /* Form Validation */
    $('#formSession')
    .formValidation({
        feedbackIcons: {
            valid: 'icon icon-check',
            invalid: 'icon icon-cross',
            validating: 'icon icon-refresh'
        },
        excluded: ':disabled', // <=== Add the excluded opti
        fields: {
            'user[email]': {
                validators: {
                    notEmpty: {
                       message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                },
            },
            'user[password]': {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 8,
                        max: 30,
                        message: 'The password must be between 8 and 30 characters long'
                    }
                },
            }
        }
    });
});