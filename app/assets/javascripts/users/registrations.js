$(document).ready(function() {
    var i18n = $('#body').attr('data-locale');
    // You don't need to care about this function
    // It is for the specific demo
    function adjustIframeHeight() {
        var $body   = $('body'),
                $iframe = $body.data('iframe.fv');
        if ($iframe) {
            // Adjust the height of iframe
            $iframe.height($body.height());
        }
    };
    $('#new_user')
    .formValidation({
        feedbackIcons: {
            valid: 'icon icon-check',
            invalid: 'icon icon-cross',
            validating: 'icon icon-refresh'
        },
        excluded: ':disabled', // <=== Add the excluded opti
        fields: {
            'user[first_name]': {
                //message: 'The user is not valid',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        message: 'The first name must be more than 2'
                    },
                }
            },
            'user[last_name]': {
                //message: 'The user is not valid',
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        message: 'The last name must be more than 2'
                    },
                }
            },
            'user[email]': {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    },
                    remote: {
                        message: 'The email is not available',
                        url: '/checking/email/',
                        onkeyup: false,
                        delay: 2000,     // Send Ajax request every 2 seconds
                        /*data: {
                            type: 'email'
                        },*/
                        type: 'POST'
                    }
                }
            },
            'user[organization_name]': {
                //message: 'The user is not valid',
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        message: 'The last name must be more than 2'
                    },
                }
            },
            'user[password]': {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must be more than 8'
                    },
                }
            },
            'user[password_confirmation]': {
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and cannot be empty'
                    },
                    identical: {
                        field: 'user[password]',
                        message: 'The password and its confirm must be the same'
                    }
                }
            }
        }
    })
    .bootstrapWizard({
        tabClass: 'nav nav-pills',
        onTabClick: function(tab, navigation, index) {
            return validateTab(index);
        },
        onNext: function(tab, navigation, index) {
            var numTabs    = $('#new_user').find('.tab-pane').length,
                isValidTab = validateTab(index - 1);
            if (!isValidTab) {
                return false;
            }

            if (index === numTabs) {
                // $('#new_user').formValidation('defaultSubmit');
                // For testing purpose
                //$('#create').show();
            }

            return true;
        },
        onPrevious: function(tab, navigation, index) {
            return validateTab(index + 1);
        },
        onTabShow: function(tab, navigation, index) {
            // Update the label of Next button when we are at the last tab
            var numTabs = $('#new_user').find('.tab-pane').length;
            if (index === numTabs - 1)
            {$('.next').hide();$('#create').show();}
            else
            {$('#create').hide();$('.next').show();}
            /*$('#new_user')
                .find('.next')
                    .removeClass('disabled')    // Enable the Next button
                    .find('a')
                    .html(index === numTabs - 1 ? 'Install' : 'Next');*/

            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            var progressBar = $('#rootwizard-1').find('.progress-bar');

            progressBar.css({width:$percent+'%'});
            // Progress Bar Color Change - Optional
            // First, remove the previous highilight-color-*
            var regex = new RegExp('\\b' + 'progress-bar-' + '.+?\\b', 'g');
            $('#rootwizard-1').find('.progress-bar')[0].className = progressBar[0].className.replace(regex, '');

            // Progress Bar Color Change - Optional
            if($percent < 20){
              progressBar.addClass("progress-bar-danger");
            }else if(($percent >= 20) && ($percent < 50)){
              progressBar.addClass("progress-bar-warning");
            }else if(($percent >= 50) && ($percent < 75)){
              progressBar.addClass("progress-bar-info");
            }else if(($percent >= 75) && ($percent < 100)){
              // Keep default progress bar color
            }else{
              progressBar.addClass("progress-bar-success");
            }
            adjustIframeHeight();
        }
    });

    $('.add_li').prop('disabled',true);
    $('#user_users_organizations_attributes_0_invitations_attributes_0_email').keyup(function(){
      $('.add_li').prop('disabled', this.value == "" ? true : false);     
    });
    $(document).off('click', ".add_li").on('click', ".add_li", function (e) {  
      var email  =  $("#user_users_organizations_attributes_0_invitations_attributes_0_email").val();
      var date   =  getDateTime();
      var verify =  false;
      
        if (validateEmail(email)) {
          $('#list_invitations > li').each(function() {
              var user = $(this).attr("data-id");
              if (user == email) {
                verify = true;
              }
          });
          if (!verify)
          { $('#user_users_organizations_attributes_0_invitations_attributes_0_email').val('');
            $('.add_li').prop('disabled',true);
            $('#list_invitations').append('<li class="list-group-item" data-id="'+email+'">'+
                                            '<div class="row">'+
                                                '<div style="padding-left: 30px" class="col-md-10">'+email+'</div>'+
                                                '<div align="center" class="col-md-2">'+
                                                    '<span style="cursor:pointer" class="remove_li icon-cross icon-size-small"></span>'+
                                                    '<input value="'+email+'" type="hidden" name="user[users_organizations_attributes][0][invitations_attributes]['+date+'][email]" id="user_users_organizations_attributes_0_invitations_attributes_'+date+'_email">'+
                                                '</div>'+
                                            '</div>'+
                                          '</li>');
          }else{toastr.error( i18n == "en" ? 'You already add this person to the list. Select another.' : 'Ya se agrego la persona a la lista. Seleccione a otra persona.', 'Error!');}
        }else{toastr.error( i18n == "en" ? 'Email invalid inva.' : 'Email Invalid.', 'Error!');}
    });
    $(document).off('click', ".remove_li").on('click', ".remove_li", function (e) {
      var li = $(this).closest('li');
      li.fadeOut(400, function(){
          li.remove();
      });
    });
    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
    function validateTab(index) {
        var fv   = $('#new_user').data('formValidation'), // FormValidation instance
            // The current tab
            $tab = $('#new_user').find('.tab-pane').eq(index);
        // Validate the container
        fv.validateContainer($tab);
        var isValidStep = fv.isValidContainer($tab);
        if (isValidStep === false || isValidStep === null) {
            // Do not jump to the target tab
            return false;
        }
        return true;
    }
    function getDateTime() {
        var now     = new Date();
        var year    = now.getFullYear();
        var month   = now.getMonth()+1;
        var day     = now.getDate();
        var hour    = now.getHours();
        var minute  = now.getMinutes();
        var second  = now.getSeconds();
        if(month.toString().length == 1) {
            var month = '0'+month;
        }
        if(day.toString().length == 1) {
            var day = '0'+day;
        }
        if(hour.toString().length == 1) {
            var hour = '0'+hour;
        }
        if(minute.toString().length == 1) {
            var minute = '0'+minute;
        }
        if(second.toString().length == 1) {
            var second = '0'+second;
        }
        var dateTime = year+''+month+''+day+''+hour+''+minute+''+second;
         return dateTime;
    }

    $('#edit_profile')
    .formValidation({
        feedbackIcons: {
            valid: 'icon icon-check',
            invalid: 'icon icon-cross',
            validating: 'icon icon-refresh'
        },
        excluded: ':disabled', // <=== Add the excluded opti
        fields: {
            'user[first_name]': {
                //message: 'The user is not valid',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        message: 'The first name must be more than 2'
                    },
                }
            },
            'user[last_name]': {
                //message: 'The user is not valid',
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        message: 'The last name must be more than 2'
                    },
                }
            },
            'user[email]': {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    },
                    remote: {
                        message: 'The email is not available',
                        url: '/checking/email/',
                        onkeyup: false,
                        delay: 2000,     // Send Ajax request every 2 seconds
                        /*data: {
                            type: 'email'
                        },*/
                        type: 'POST'
                    }
                }
            },
            'user[organization_name]': {
                //message: 'The user is not valid',
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        message: 'The last name must be more than 2'
                    },
                }
            },
            'user[password]': {
                validators: {
                    stringLength: {
                        min: 8,
                        message: 'The password must be more than 8'
                    },
                }
            },
            'user[password_confirmation]': {
                validators: {
                    identical: {
                        field: 'user[password]',
                        message: 'The password and its confirm must be the same'
                    }
                }
            }
        }
    });

});
