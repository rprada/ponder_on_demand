var modal_holder_selector, modal_selector;
modal_holder_selector = '#modal-holder';
modal_selector = '.modal';
$(document).off('click', "a[data-modal]").on('click', "a[data-modal]", function (e) {
  var location;
  location = $(this).attr('href');
  $.get(location, function(data) {
    return $(modal_holder_selector).html(data).find(modal_selector).modal();
  });
  return false;
});