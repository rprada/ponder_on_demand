var FieldsQuestions = $('#questions')
                      .children('.fieldsQuestions')
var numFieldsQuestions = FieldsQuestions.length
var fieldGlobal = null

var i18n = $('#body').attr('data-locale'); 
/* Registration Form */
$('#formScorecard')
.formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok-circle',
        invalid: 'glyphicon glyphicon-remove-circle',
        validating: 'glyphicon glyphicon-refresh'
    },
    //excluded: ':disabled', // <=== Add the excluded opti
    excluded: [':disabled', ':hidden', ':not(:visible)'],    
    fields: {
      'scorecard[pass_grade]': {
          validators: {
              notEmpty: {
                  message: i18n == "en" ? 'Pass grade is required and cannot be empty' : 'Nombre es obligatorio y no puede estar vacío'
              },
          }
      },
      'scorecard[num_questions]': {
          validators: {
            notEmpty: {
                message: i18n == "en" ? 'Num Questions is required and cannot be empty' : 'N° de preguntas es obligatorio y no puede estar vacío'
            },
            callback: {
              callback: function(value, validator, $field) {
                var value = $field.val();
                var different_question = $('#formScorecard').find('input[name="scorecard[different_question_order]"]').iCheck('update')[1].checked;
                if (different_question == true) 
                {if (value != numFieldsQuestions)
                  { return{
                      valid: false,
                      message: i18n == "en" ? 'Error no son iguales' : 'Ningún usuario puede estar en reportar a y administra a al mismo tiempo'
                    }
                  }
                }
                return true;
              }
            },
          }
      }
    }
})
.on('keyup', '[name="scorecard[num_questions]"]', function() {
  $('#formScorecard').formValidation('revalidateField', 'scorecard[num_questions]');
})  
.on('keyup', '[name="scorecard[pass_grade]"]', function() {
  $('#formScorecard').formValidation('revalidateField', 'scorecard[pass_grade]');
})        
        

$('#small-modal').appendTo("body") 
$(document).off('click', "#updateConfirm").on('click', "#updateConfirm", function (e) {
    var id = $("#scorecard_id").val();
    var request_category = $("#scorecard_request_category_id").val();
    var num_questions = $("#scorecard_num_questions").val();
    var different = $('input[name="scorecard[different_question_order]"]').iCheck('update')[1].checked;
    if( $("#formScorecard").data('formValidation').validate().isValid()) 
    {if (different == true )
      {if (numFieldsQuestions == num_questions )
        { e.preventDefault();
          $('#small-modal').modal('show');
          var message = $('#modal_save').val();
          $('.modal-body').empty();
          $('.modal-body').append(message);
          var $submit = $('#small-modal').find('.btn-primary');
          $submit.attr('id','update');
        }
        else
        { var span  = $("#scorecard_num_questions").parents('.col-md-3')
                      .children('span');
          $("#scorecard_num_questions").css("border", "1px solid red");
          $(span).show();$(span).empty();$(span).append('Error');
        }
      }
      else
      {
        e.preventDefault();
        $('#small-modal').modal('show');
        var message = $('#modal_save').val();
        $('.modal-body').empty();
        $('.modal-body').append(message);
        var $submit = $('#small-modal').find('.btn-primary');
        $submit.attr('id','update');
      }
    }
});

$(document).off('click', "#update").on('click', "#update", function () {
    var id = $("#scorecard_id").val();
    var formData = new FormData($("#formScorecard")[0]);
    $.ajax({
       url: '/scorecards/' + id,
       type: 'PUT',
       data: formData,
       dataType: 'JSON',               
       cache: false,
       processData: false,
       contentType: false,
      beforeSend: function(){
        $("#small-modal").modal("hide");
        //$.fancybox.showLoading();                      
      },                
      success: function(object) {
        //$.fancybox.hideLoading();
          if ( object.success ) 
          {//Turbolinks.visit('/' + i18n + '/scorecards/'+id+'/edit');
           setTimeout(function() { 
              toastr.success(object.message[0], object.message[1]);
           }, 1500);}
          else{
          setTimeout(function() { 
              toastr.error(object.message[0], object.message[1]);
          }, 1500);}
      }
    });
});

$('.chosen-select').chosen({
    //allow_single_deselect: true,
    //no_results_text: 'Ningún resultado coincide con',
    placeholder_text: 'Select a Question Type',
    width: "100%",
});      
$('#save').on("click", function() {
    if( $("#formScorecard").data('formValidation').validate().isValid() ) {
       if (confirm("Sure To Save Scorecard?")) 
        {   var formData = new FormData($("#formScorecard")[0]);
            $.ajax({
                url: '/request_categories/2/scorecards',
                type: 'POST',
                data: formData,
                dataType: 'JSON',               
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                  //$.fancybox.showLoading();                        
                },                
                success: function(resp) {
                  if ( resp.success ) 
                  {//$.fancybox.hideLoading();
                  }
                },
            });
        }
    }               
});

$(document).off('click', ".remove_fields").on('click', ".remove_fields", function () {
  var father = $(this).parents('.fieldsQuestions')
                        .children('#answers')
                        .children('.saveQuestion');
  $(father).show();

  var father2     =   $( this ).parents('.nested-fields');
  var textInfo    =   $( father2 ).children('#answers')
                                  .children('.textInfo')

  var count = $('.fieldsAnswers:not([style*="display: none"])').length
  if( count == 1 ){$(textInfo).hide();}

});

$(document).off('change', "input:checkbox").on('change', "input:checkbox", function () {
//$('input:checkbox').change(function () {
  var father = $(this).parents('.fieldsQuestions')
                      .children('#answers')
                      .children('.saveQuestion');
  $(father).show();
});
$(document).off('change', "input[type=radio]").on('change', "input[type=radio]", function () {
  var father = $(this).parents('.fieldsQuestions')
                      .children('#answers')
                      .children('.saveQuestion');
  $(father).show();
});

$('input').on('ifChanged', function(event){
  var father = $(this).parents('.fieldsQuestions')
                      .children('#answers')
                      .children('.saveQuestion');
  $(father).show();
});

$(document).off('keyup', "#scorecard_pass_grade").on('keyup', "#scorecard_pass_grade", function () {
  var span = $(this).parents('.col-md-8')
                    .children('span');
  var string  = $(this).val(); 
  var myInput = string.slice(0,-1);
  if ( myInput === "" )
  { $(this).css("border", "1px solid red");
    $(span).show();$(span).empty();$(span).append('Pass grade is required and cannot be empty');}
    else{$(this).css("border", "1px solid green");$(span).hide();}
});

$(document).off('input', "input").on('input', "input", function () {
  var question =  $( this ).parents('.nested-fields')
                  .children('.field')
                  .children('.col-md-9')
                  .children('input');
  var typeQues =  $( this ).parents('.nested-fields')
                  .children('.field')
                  .children('.col-md-4')
                  .children('select');
  var spanQ    =  $( this ).parents('.nested-fields')
                  .children('.field')
                  .children('.col-md-9')
                  .children('span');
  var answers  =  $(question).parents('.nested-fields')
                  .children('#answers')
                  .children('.fieldsAnswers'); 
  var button   =  $(question).parents('.nested-fields')
                  .children('#answers')
                  .children('.saveQuestion');

  if ( $(question).val() == "" )
  {$(question).css("border", "1px solid red");                                
   $(spanQ).show();$(spanQ).empty();$(spanQ).append('Question is required and cannot be empty');}
  else{$(spanQ).hide();$(question).css("border", "1px solid green");}
  var cont = 0;
  for (var i = 0; i < $(answers).length; i++) {
      var answer = $(answers[i]).children('.field')
                   .children('.col-md-7')
                   .children('input');
      var span   = $(answers[i]).children('.field')
                   .children('.col-md-7')
                   .children('span');
      if ($(answer).val() == "")
      {$(answer).css("border", "1px solid red"); 
       $(span).show();$(span).empty();$(span).append('Answer is required and cannot be empty');
       $(button).hide();}
      else{$(answer).css("border", "1px solid green");cont += 1;$(span).hide();}
  }
  if (cont === answers.length && $(question).val() !== "" && $(typeQues).val() !== "")
  {$(button).show();}else{$(button).hide();}  
});


$('#questions')
.on('cocoon:after-insert', function () {
  var count  =  0;
  var check  =  changeCheckbox.checked
  var simple = $("#type_entity_simple").val();
  if( check === true ){
    $(".question_type option[value=" + simple + "]").css('display','none');
    $('.question_type').trigger("chosen:updated");
  }
  $(".score").each(function(index) {
    count += 1;
    //$( this ).parents('.nested-fields').attr('data-pos',index)
  }); 

  var score = 100/count
  $(".score").each(function(index) {
    $(this).val(score.toFixed(2));
  });  
});

$('#questions')
.on('cocoon:before-remove', function () {
});

$('#small-modal').appendTo("body") 
$('#delete-confirm').appendTo("body") 
$(document).off('click', ".deleteConfirm").on('click', ".deleteConfirm", function (e) {
    e.preventDefault();
    fieldGlobal  =  $( this ).parents('.nested-fields')
    $('#delete-confirm').modal('show');
    $('#delete-confirm').attr('data-id', $(this).data('id')).modal('show');
    var message = $('#modal_delete').val();
    $('.modal-body').empty();
    $('.modal-body').append(message);
});
$(document).off('click', "#removeQuestions").on('click', "#removeQuestions", function () {
    numFieldsQuestions = numFieldsQuestions - 1;
    $(".score").val(100/numFieldsQuestions);
    var id               =  $("#scorecard_id").val();
    var request_category =  $("#scorecard_request_category_id").val(); 
    var question         =  $( this ).parents('.questions');      
    var father           =  $( fieldGlobal ).children('.field')
                                            .children('.col-md-9')
                                            .children('input'); 
    var mystring  =  $(father).attr('id');
    var string    =  $(father).attr('id');
    var str  = mystring.substring(mystring.indexOf("butes_")+6);
    var str2 = str.substr(0, str.indexOf('_'));
    var formData = new FormData($("#formScorecard")[0]);
      $.ajax({
           url:  '/scorecards/question/' + id + "/" + str2,
           type: 'put',
           data: formData,
           dataType: 'JSON',               
           cache: false,
           processData: false,
           contentType: false,
          beforeSend: function(){
            $("#delete-confirm").modal("hide");
            //$.fancybox.showLoading(); 
          },                
          success: function(object) {
            //$.fancybox.hideLoading();
            if ( object.success ) 
            {toastr.success(object.message[0], object.message[1]);}
            else{toastr.error(object.message[0], object.message[1]);}
          },
          complete: function(){
            $(fieldGlobal).remove();
            fieldGlobal = null;
            var clsElements = document.querySelectorAll('.fieldsQuestions');
            if( clsElements.length < 1 ){$('.newQuestionTwo').hide()};            
          },
      });
});

$(document).off('click', "#updateQuestion").on('click', "#updateQuestion", function () {
  var id = $("#scorecard_id").val();
  var request_category = $("#scorecard_request_category_id").val();  
  var index   =    $( this ).parents('.nested-fields').attr("data-pos");
  var father  =    $( this ).parents('.nested-fields')
                            .children('.field')
                            .children('.col-md-9')
                            .children('input');

  var childrenAnswers     =  $(father).parents('.nested-fields')
                                      .children('#answers')
                                      .children('.fieldsAnswers:visible');

  var childrenAnswersNone =  $(father).parents('.nested-fields')
                                      .children('#answers')
                                      .children(".fieldsAnswers[style$='display: none;']");

  var buttonSaveQuestion  =  $(father).parents('.nested-fields')
                                      .children('#answers')
                                      .children('.saveQuestion');

  var mystring  =  $(father).attr('id'),
           str  = mystring.substring(mystring.indexOf("butes_")+6),
           str2 = str.substr(0, str.indexOf('_'));

  $(father).val() == "" ? $(father).css("border", "1px solid red") : $(father).css("border", "1px solid green")

  var cont  = 0;
  var cont2 = 0;
  for (var i = 0; i < $(childrenAnswers).length; i++) {
      var answer = $(childrenAnswers[i]).children('.field')
                                        .children('.col-md-7')
                                        .children('input');
      var span   = $(childrenAnswers[i]).children('.field')
                                        .children('.col-md-7')
                                        .children('span');      
      var check  = $(childrenAnswers[i]).children('.field')
                                        .children('.check');

    if ($(answer).val() == "")
    {$(answer).css("border", "1px solid red"); 
     $(span).show();$(span).empty();$(span).append('Answer is required and cannot be empty')}
    else{$(answer).css("border", "1px solid green"); cont += 1;}

    /*var radio = $('input:radio[name="scorecard[questions_attributes][answers_attributes][correct_answer]"]:checked');
    if(radio.length == 0 )                                      
    {$(check).css("border", "1px solid red");}
    else{$(check).css("border", "1px solid green"); cont2 += 1;}*/

  }

  if ((cont === childrenAnswers.length /*&& cont2 === childrenAnswers.length*/) && ($(father).val() !== "" ))
  { 
    var all_input = $( this ).parents('.nested-fields').find('input');
    var answer_input = $( this ).parents('.nested-fields').find('input.idAnswer');

    var formData = new FormData($("#formScorecard")[0]);
    $.ajax({
         url: '/scorecards/' + id + "/" + str2,
         type: 'PUT',
         data: formData,
         dataType: 'JSON',               
         cache: false,
         processData: false,
         contentType: false,
        beforeSend: function(){
          //$.fancybox.showLoading(); 
        },                
        success: function(obj) {
          console.log(obj);
          if ( obj.success ) 
          {//$.fancybox.hideLoading();
            $("#scorecard_questions_attributes_"+str2+"_id").val(obj.id);
            $(childrenAnswersNone).nextAll('input[type="hidden"]').remove();
            $(childrenAnswersNone).remove();
            setTimeout(function() { 
              toastr.success(obj.message[0], obj.message[1]);
           }, 1500);
            $(father[1]).val(obj.object.id); //ASIGNA ID A INPUT QUESTION_ID 
            for (var i = 0; i < $(childrenAnswers).length; i++) {
              var answer = $(childrenAnswers[i]).children('.field')
                                                .children('.idAnswer')
                                                .children('input.id_');
              answer.val(obj.object[i].id);
            }
            for (var i = 0; i < all_input.length; i++) {
                $(all_input[i]).css("border","");  
            }
          }else{
            setTimeout(function() { 
                toastr.error(obj.message[0], obj.message[1]);
            }, 1500);
          } $(buttonSaveQuestion).hide();
        },
    });
  //}
  }
});
$('#small-modal').appendTo("body") 
$(document).off('click', "#cancelConfirm").on('click', "#cancelConfirm", function (e) {
  e.preventDefault();
  $('#small-modal').modal('show');
  var message = $('#modal_cancel').val();
  $('.modal-body').empty();
  $('.modal-body').append(message);
  var $submit = $('#small-modal').find('.btn-primary');
  $submit.attr('id','cancel');
});
$(document).off('click', "#cancel").on('click', "#cancel", function () {
  Turbolinks.visit( '/' + i18n + '/request_categories/' );
});
 
$("#scorecards").DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": $('#scorecards').data('source'),
    "pagingType": 'simple_numbers',
    autoWidth: false,
    "bLengthChange": false,
    "iDisplayLength": 8, // Number of rows to display on a single page when using pagination.
    "order": [[ 0, "desc" ]],
    columnDefs: [
       { orderable: false, targets: -1 }
    ]
});

changeCheckbox.onchange = function() {
  var simple = $("#type_entity_simple").val();
  var cont = [];
  if( changeCheckbox.checked === true ){
     $("#scorecard_pass_grade").removeAttr("disabled");
     $("#scorecard_pass_grade").val('0%');
     $(".question_type option[value=" + simple + "]").css('display','none');
     $('.question_type').trigger("chosen:updated");
     var fieldsQuestions = $('.question_type').parents(".fieldsQuestions");     
     for (var i = 0; i < fieldsQuestions.length; i++) {
        var select = $(fieldsQuestions[i]).children('.field')
                                          .children('.col-md-4')
                                          .children('select')
      if ( $(select).val() === simple ){cont.push(i);} //estudiar los casos de questionsType preguntar a cesar
    }
     if (cont.length > 0) 
     {if (confirm("Sure To Change Gradeable?")) 
      {for (var i = 0; i < cont.length; i++) {
        var input = $(fieldsQuestions[cont[i]]).children('.field')
                                               .children('.col-md-9')
                                               .children('input')          
        var mystring = $(input).attr('id');
        var str      = mystring.substring(mystring.indexOf("butes_")+6);
        var str2     = str.substr(0, str.indexOf('_'));
        var input_id = $("#scorecard_questions_attributes_"+str2+"_id").val();
        $.ajax({
        url: '/' + i18n + '/questions/' + input_id ,
        type: 'delete',
          success: function(object) {
            if ( object.success ) 
                toastr.success(object.message[0], object.message[1]);
            else{
                toastr.error(object.message[0], object.message[1]);
           }
          },
        });
        $(fieldsQuestions[cont[i]]).remove()
        numFieldsQuestions = numFieldsQuestions - 1;        
       }//$('#scorecard_gradable').bootstrapSwitch('toggleState'); //bootstrapSwitch('setState', true); // true || false
      }else{
        $('#formScorecard').formValidation('resetField', 'scorecard[pass_grade]');
        $("#scorecard_pass_grade").prop("disabled", true);
        $("#scorecard_pass_grade").val(' ');
        changeCheckbox.checked = !changeCheckbox.checked;
        changeCheckbox.checked === false ? init2.handleOnchange(changeCheckbox.checked) : "";
      }
     }
    }
    else { if( changeCheckbox.checked === false){      
     $('#formScorecard').formValidation('resetField', 'scorecard[pass_grade]');
     $("#scorecard_pass_grade").prop("disabled", true);
     $("#scorecard_pass_grade").val(' ');
     $(".question_type option[value=" + simple + "]").css('display','block');
     $('.question_type').trigger("chosen:updated");
   }}
};

if ( $('#scorecard_gradable').size() > 0 )
{ if( changeCheckbox.checked === true )
  {$("#scorecard_pass_grade").removeAttr("disabled");
   var simple = $("#type_entity_simple").val();
   $(".question_type option[value=" + simple + "]").css('display','none');
   $('.question_type').trigger("chosen:updated");
  }else{$("#scorecard_pass_grade").prop("disabled", true);
       $("#scorecard_pass_grade").val(' ');}
}

changeDifferentQuestion.onchange = function() {
  if( changeDifferentQuestion.checked === true ){
    if($('input[name="scorecard[different_question_order]"]').val() == "true"){
      $("#scorecard_num_questions").removeAttr("disabled");}
  $('input[name="scorecard[different_question_order]"]').iCheck('enable');
  $('input[name="scorecard[different_question_order]"]').iCheck('check');} 
  else{ if( changeDifferentQuestion.checked === false ){
     $('#formScorecard').formValidation('resetField', 'scorecard[num_questions]');
     $('input[name="scorecard[different_question_order]"]').iCheck('disable');
     $('input[name="scorecard[different_question_order]"]').iCheck('uncheck');
     $("#scorecard_num_questions").prop("disabled", true);
     $("#scorecard_num_questions").val(' ');
     $("#scorecard_num_questions").css('border', '');
     $("#scorecard_num_questions").parents('.col-md-3').children('span').empty();}}
}

if ( $('#scorecard_type_different_question').size() > 0 )
{ if( changeDifferentQuestion.checked === true )
  {$('input[name="scorecard[different_question_order]"]').iCheck('enable');}
  else{ $('input[name="scorecard[different_question_order]"]').iCheck('disable');
        $('input[name="scorecard[different_question_order]"]').iCheck('uncheck');
        $('#scorecard_num_questions').iCheck('uncheck');
        $("#scorecard_num_questions").prop("disabled", true);
        $("#scorecard_num_questions").val(' ');}
}

var valid = $('input[name="scorecard[different_question_order]"]').iCheck('update')[1].checked ;
if(valid == false)
{$('#scorecard_num_questions').iCheck('uncheck');
 $("#scorecard_num_questions").prop("disabled", true);
 $("#scorecard_num_questions").val(' ');}  

if($(".icheck-me").length > 0){
    $(".icheck-me").each(function(){
      var $el = $(this);
      var skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
      color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
      var opt = {
        checkboxClass: 'icheckbox' + skin + color,
        radioClass: 'iradio' + skin + color,
      }
      $el.iCheck(opt);
    });
}
$('input[name="scorecard[different_question_order]"]').on('ifChecked', function(event,state){
    if ( $(this).val() === "true" )
    {$("#scorecard_num_questions").removeAttr("disabled");}
    else{$('#formScorecard').formValidation('resetField', 'scorecard[num_questions]');
         $("#scorecard_num_questions").prop("disabled", true);
         $("#scorecard_num_questions").val(' ');
         $("#scorecard_num_questions").css('border', '');
         $("#scorecard_num_questions").parents('.col-md-3').children('span').empty();}
});

if ($('#answers').size() > 0 )
{
  var father      =   $( '#answers' ).parents('.fieldsQuestions');
  var child       =   $( father  ).children().children().children('input');

  var mystring    =   $(child).attr('id');
  var str         =   mystring.substring(mystring.indexOf("butes_")+6);
  var str2        =   str.substr(0, str.indexOf('_'));
}

/*$('#answers')
.on('cocoon:after-insert', function () {*/
 $(document).off('click', ".newAnswer").on('click', ".newAnswer", function (event) {
  var father      =   $(  this  ).parents('.nested-fields');
  var more        =   $(father).children('.field').children().children('.more_answers')
  var input       =   $( more ).children().children('input');
  var textInfo    =   $( father ).children('#answers')
                                 .children('.textInfo');  
  var fieldAnswer =   $( father ).children('#answers')
                                 .children('.fieldsAnswers');
  var div_score   =   $( fieldAnswer ).children('.field')
                                      .children('.score')
                                      .children('input');

  var check = $(input).iCheck('update')[0].checked;

  if ( check === true ){
    $(father).children('#answers').children('.nested-fields')
                                  .children().children('.only_checkbox').show();
    $(father).children('#answers').children('.nested-fields')
                                  .children().children('.only_checkbox')
                                  .children('.chk_check').prop('disabled', false);

    $(father).children('#answers').children('.nested-fields')
                                  .children().children('.only_radio').hide();
    $(father).children('#answers').children('.nested-fields')
                                  .children().children('.only_radio')
                                  .children('.chk_radio').prop('disabled', true);

  }else{
    $(father).children('#answers').children('.nested-fields')
                                  .children().children('.only_checkbox').hide();
    $(father).children('#answers').children('.nested-fields')
                                  .children().children('.only_checkbox')
                                  .children('.chk_radio').prop('disabled', true);

    $(father).children('#answers').children('.nested-fields')
                                  .children().children('.only_radio').show();
    $(father).children('#answers').children('.nested-fields')
                                  .children().children('.only_radio')
                                  .children('.chk_radio').prop('disabled', false);
  } 
  $(textInfo).show();
  var clsElements = document.querySelectorAll('.fieldsQuestions');

})

$('.newQuestion').on('click', function(){
  $('.newQuestionTwo').show();
});
//$('.more').on('ifChecked', function(event){
$(document).off('ifChecked', ".more").on('ifChecked', ".more", function (event) {

    var mystring = $(this).attr('id');
    var str      = mystring.substring(mystring.indexOf("butes_")+6);
    var str2     = str.substr(0, str.indexOf('_'));

    var father   = $( "#"+mystring+"" ).parents('.nested-fields');
    var child    = $( father ).children('#answers'); //#answer;
    var child2   = $( child ).children('.nested-fields'); //#answer;
    var textInfo = $( child ).children('.textInfo'); 

    $(child2).children().children('.only_radio').hide();
    $(child2).children().children('.only_radio').children('.chk_radio').remove();

    var input_hidden = $(child2).children().children('.only_checkbox').children('input[type=hidden]');
    var input_check  = $(child2).children().children('.only_checkbox');
    var input_checkbox = $(child2).children().children('.only_checkbox').children('input[type=checkbox]');
    input_checkbox.size() > 0 ? input_checkbox.remove() : "";

    $(input_hidden).removeAttr("disabled")

    $(input_hidden).each( function(index, value){
      var string = $(this).attr('name');
      var str3        = string.substring(string.indexOf("ns_attributes][")+15);
      var question_id = str3.substr(0, str3.indexOf(']'));
      var str4        = string.substring(string.indexOf("rs_attributes][")+15);
      var answer_id   = str4.substr(0, str4.indexOf(']'));
      $(input_check[index]).show().append('<input style="margin: 10px 0 0 0px" class="chk_check" type="checkbox" value="1" name="scorecard[questions_attributes]['+question_id+'][answers_attributes]['+answer_id+'][correct_answer]" id="scorecard_questions_attributes_'+question_id+'_answers_attributes_'+answer_id+'_correct_answer">');
    })
});
$(document).off('ifUnchecked', ".more").on('ifUnchecked', ".more", function (event) {
//$('.more').on('ifUnchecked', function(event){
  var mystring = $(this).attr('id');
  var str      = mystring.substring(mystring.indexOf("butes_")+6);
  var str2     = str.substr(0, str.indexOf('_'));

    var father   = $( "#"+mystring+"" ).parents('.nested-fields');
    var child    = $( father ).children('#answers'); //#answer;
    var child2   = $( child ).children('.nested-fields'); //#answer;
    var textInfo = $( child ).children('.textInfo'); 

    $(child2).children().children('.only_checkbox').hide();
    //$(child2).children().children('.only_radio').show();
    $(child2).children().children('.only_checkbox').children('.chk_check').remove();

    var input_hidden = $(child2).children().children('.only_checkbox').children('input[type=hidden]');
    var input_checkbox = $(child2).children().children('.only_radio').children('input[type=radio]');
    input_checkbox.size() > 0 ? input_checkbox.remove() : "";
    $(input_hidden).attr("disabled", true)
    
    var input_radio  = $(child2).children().children('.only_radio');
    $(input_hidden).each( function(index, value){
      var string = $(this).attr('name');
      var str3        = string.substring(string.indexOf("ns_attributes][")+15);
      var question_id = str3.substr(0, str3.indexOf(']'));
      var str4        = string.substring(string.indexOf("rs_attributes][")+15);
      var answer_id   = str4.substr(0, str4.indexOf(']'));    
      $(input_radio[index]).show().append('<input style="margin: 10px 0 0 0px" class="chk_radio" name="scorecard[questions_attributes]['+question_id+'][answers_attributes]['+answer_id+'][correct_answer]" id="scorecard_questions_attributes_'+question_id+'_answers_attributes_'+answer_id+'_correct_answer" type="radio" value="true">');                                    

    })
});

$(document).off('click', ".chk_radio").on('click', ".chk_radio", function () {
  var mystring = $(this).attr('id');
  var myname   = $(this).attr('name');
  var str      = mystring.substring(mystring.indexOf("butes_")+6);
  var str2     = str.substr(0, str.indexOf('_'));
  var father   = $( "#"+mystring+"" ).parents('.nested-fields');
  var child2   = $( child ).children('.nested-fields'); //#answer;

  var child    = $( father[1] ).children('#answers')
                               .children('.nested-fields')
                               .children('.field')
                               .children('.col-md-1')
                               .children('input[type=radio].chk_radio')
  $(child).not(this).removeAttr("checked");

  var child_input   =  $( father[1] ).children('#answers')
                                     .children('.nested-fields')
                                     .children('.field')
                                     .children('.col-md-1')
                                     .children('input[type=text].chk_hidden')   
  $(child_input).not(this).val('false');
  $( 'input[type=text][name="'+myname+'"]' ).val('true');

});

$(document).off('change', ".question_type2").on('change', ".question_type2", function () {

    var id = $(this).val();
    $.ajax({ 
         url : '/scorecards/'+ id +'/questionType',
         type: 'get',
         data: id,
        success: function(resp) {

        },

    });
});


$(document).off('change', ".question_type").on('change', ".question_type", function () {
    var questionType = $("option:selected", this).text();
    var mystring = $(this).attr('id');
    var str  = mystring.substring(mystring.indexOf("butes_")+6);
    var str2 = str.substr(0, str.indexOf('_'));
    var father  = $( "#"+mystring+"" ).parents('.nested-fields');
    var child   = $( father ).children('#answers'); //#answer;
    var child2  = $( child ).children('#links'); //#links;
      
    var links1  = $( child ).children('.1'); //#answer;
    var links2  = $( child ).children('.2'); //#answer;

    var button1 = $( links1 ).children('a.add_fields'); //#answer;
    var button2 = $( links2 ).children('a.add_fields'); //#answer;

    var div_more_as      =  $( father ).children('.field').children().children('.more_answers');
    var input_more_as    =  $( div_more_as ).children().children('input');
    var div_random_ord   =  $( father ).children('.field').children().children('.random_order');
    var input_random_ord =  $( div_random_ord ).children().children('input');

    if ( questionType === "Multiple Options" || questionType === "Combo")        
    { var child_nested = $(child).children('.nested-fields');
      $( child_nested ).remove();
      $( child.children('.multiple_options') ).remove();
      $( child.children('.yes_not') ).remove();
      $(button1).trigger('click');
      $(button1).trigger('click');
      $(button1).trigger('click');
      $(links1).show();
      $(div_more_as).show();
      $(div_random_ord).show();
      $(links2).hide();
      $(input_more_as).iCheck('uncheck');
      $(input_random_ord).iCheck('uncheck'); 

      var textInfo    =   $( child ).children('.textInfo');
      $(textInfo).show()
    
    }else
    { 
      if (questionType === "Yes/No")
      {
        var child_nested = $(child ).children('.nested-fields')
        $( child_nested ).remove();
        $( child.children('.multiple_options') ).remove();
        $(button2).trigger('click');        
        $(button2).trigger('click');
        var input_yes_not = $(child).children('.nested-fields').children('.field').children('.yes_no').children('input[id$=content]');
        $(div_more_as).hide();
        $(div_random_ord).hide();        
        $(input_more_as).iCheck('uncheck');
        $(input_random_ord).iCheck('uncheck');
        $(links1).hide();        
        $(input_yes_not[0]).val( "Yes" );
        $(input_yes_not[1]).val( "No" );

        var textInfo    =   $( child ).children('.textInfo');
        $(textInfo).show()
      } 
      else{
          var child_nested = $(child ).children('.nested-fields')
          $( child_nested ).remove();
          //$( child.children('.multiple_options') ).remove();
          $(links1).hide();
          $(links2).hide();
          $(div_more_as).hide();
          $(div_random_ord).hide();        
          $(input_more_as).iCheck('uncheck');
          $(input_random_ord).iCheck('uncheck');  

          var textInfo    =   $( child ).children('.textInfo');
          $(textInfo).hide()

          var button   =  $(child).parents('.nested-fields')
                          .children('#answers')
                          .children('.saveQuestion');

          var input  =   $(child).parents('.nested-fields')
                         .children('.field')
                         .children('.col-md-9')
                         .children('input');          
          if ($(input).val() !=="")
          {$(button).show();}



      }
    }
});