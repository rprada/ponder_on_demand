class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
      # user ||= User.new() # guest user (not logged in)
      # organization ||= user.organizations.first
      # if UserOrganization.where(organization_id: organization.id).where( user_id: user).first.role == "Administrator"
      #   can :manage, :all
      # elsif UserOrganization.where(organization_id: organization.id).where( user_id: user).first.role == "member-owner"
      #   can :read, :all
      #   can :update, User
      #   can [:update, :destroy, :create], [Organization, Role, TypeEntity, StatusEntity]
      #   can :datatable_index, :all
      # elsif UserOrganization.where(organization_id: organization.id).where( user_id: user).first.role == "member"
      #   can :read, :all
      #   can :datatable_index, :all
      # else
      #   can :read, :all
      # end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
