class RequestsCategory < ActiveRecord::Base
  acts_as_paranoid
  #Validations

  #Relationship
  has_one :scorecard, dependent: :destroy
  belongs_to :status_entity #, :dependent => :destroy
  accepts_nested_attributes_for :scorecard

  before_validation :load_uuid
  
  private
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
end
