class Role < ApplicationRecord
  acts_as_paranoid
  belongs_to :status_entity

  has_many :users_organizations

  validates :uuid, :sort_name, :class_name,  presence: true #:created_by_uuid, presence: true
  validates :sort_name,  length: { in: 2..6 }

    #before
  before_validation :load_uuid

  private
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
end
