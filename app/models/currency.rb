class Currency < ActiveRecord::Base
  acts_as_paranoid

  #Relationship
  belongs_to :status_entity #, :dependent => :destroy
  has_and_belongs_to_many :country

  #before
  before_destroy { country.clear }
  before_validation :load_uuid

  #scope :by_company, -> (company) { where(company_id: company.id) }
  scope :by_company, -> (company) {
    joins(country: :organizations).
    where("organizations.company_id = ?", company)
    group("currencies.id")
  } 

  private
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
end