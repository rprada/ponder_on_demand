class UsersOrganization < ApplicationRecord
  acts_as_paranoid
  attr_accessor :managed_by

  belongs_to :status_entity
  belongs_to :user
  belongs_to :organization

  has_one  :users_organizations_role
  has_many :invitations, foreign_key: 'sent_by_id'
  accepts_nested_attributes_for :invitations, reject_if: lambda { |a| a[:email].blank? }, :allow_destroy => true
  
  accepts_nested_attributes_for :organization
  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :users_organizations_role

  validates :uuid, length: { in: 6..120 } #:created_by_uuid

  #before
  before_validation :load_uuid
  before_validation :set_status, on: :create
  scope :by_user, ->(user) { where(user_id: user.id).first } 
  scope :by_organiz, ->(organization) {
    select("users.full_name, users_organizations.id").
    joins(:user).
    where(organization_id: organization )
  }
  def self.managed_by users_organization_id
    select("users.full_name, users_organizations.id").
    joins(:user).
    where(users_organization_id: users_organization_id )    
  end
  def self.current
    Thread.current[:user_organization]
  end
  def self.current=(user_organization)
    Thread.current[:user_organization] = user_organization
  end

  private
    def  load_uuid
      self.uuid = SecureRandom.uuid
    end
    def set_status
      @status  = StatusEntity.by_class('UsersOrganization', 'Active')
      self.status_entity_id = @status[0].id
    end
end
