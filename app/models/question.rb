class Question < ActiveRecord::Base
  acts_as_paranoid
  #Validations
  validates  :content, :uuid, presence: true #:score, :random_answer_order, presence: true
  validates  :uuid, length: { in: 6..120 } #:created_by_uuid 

  belongs_to :scorecard
  has_many   :answers, dependent: :destroy
  accepts_nested_attributes_for :answers, reject_if: lambda { |a| a[:content].blank? }, allow_destroy: true
  
  #before
  before_save       :remove_answer
  before_save       :put_score
  after_create      :set_global
  after_update      :set_global
  before_destroy    :put_score
  before_validation :load_uuid

  scope :get_answers, -> (question) {
    select("answers.id, answers.content, answers.correct_answer,
            answers.question_id, answers.score").
    joins(:answers).
    where("answers.question_id = ? ", question )
  }

  private
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
  def remove_answer
    #unless self.request_category_id.new_record?
      if self.type_entity_id != self.type_entity_id_was
        Answer.where(question_id: self.id).destroy_all
      end
    #end
  end
  def put_score
    Question.where(scorecard_id: self.scorecard_id).update_all(score: self.score)
  end
  def set_global
    $question_id_global = self.id
  end
end
