class StatusEntity < ApplicationRecord
  acts_as_paranoid

  #Validations
  validates :uuid, :sort_name, :name, :class_name,  :created_by_uuid, presence: true
  #validates :uuid, :created_by_uuid , length: { in: 6..120 }
  validates :name, length: {in: 2..100}
  validates :sort_name,  length: { in: 2..6 }
  #validates :created_by_uuid, :created_at, presence: true

  #Relationship
  has_many :users,         dependent: :destroy
  has_many :type_entities, dependent: :destroy
  has_many :organizations, dependent: :destroy

  #before
  before_validation :load_uuid
  scope :by_class, ->(class_name, name) { where(class_name: class_name, name: name, status: true ) }

  #scope :actived, ->(class_name) {where(sort_name: 'ACTV', class_name: class_name )}
  private
  def load_uuid
    self.uuid = SecureRandom.uuid
  end
end
