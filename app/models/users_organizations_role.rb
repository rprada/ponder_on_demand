class UsersOrganizationsRole < ApplicationRecord
 
  acts_as_paranoid
  belongs_to :role
  belongs_to :users_organization

  validates :uuid, length: { in: 6..120 } #:created_by_uuid
  #before
  before_validation :load_uuid
  before_validation :set_status, on: :create

  private
    def load_uuid
      self.uuid = SecureRandom.uuid
    end
    def set_status
      @status  = StatusEntity.by_class('UsersOrganization', 'Active')
      self.status_entity_id = @status[0].id
    end
end