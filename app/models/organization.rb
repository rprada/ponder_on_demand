class Organization < ApplicationRecord
  acts_as_paranoid


  belongs_to :status_entity

  has_many :users_organizations
  has_many :users, through: :users_organizations

  #has_many :invitations #, through: :users_organizations
  #has_many :users, through: :invitations

  #accepts_nested_attributes_for :users_organizations


  #has_many :users
  #has_many :organizations
  #has_many :invitations
  #has_many :users, through: :invitations

  has_many :user_organizations
  has_many :users, through: :user_organizations

  validates :uuid, length: { in: 6..120 } #:created_by_uuid

    #before
  before_validation :load_uuid
  private
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
end
