class State < ApplicationRecord
  acts_as_paranoid

  belongs_to :status_entity
  belongs_to :country

  has_many :counties
  has_many :cities

  validates :uuid, length: { in: 6..120 } #:created_by_uuid

    #before
  before_validation :load_uuid
  private
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
end
