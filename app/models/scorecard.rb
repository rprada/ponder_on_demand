class Scorecard < ActiveRecord::Base
  acts_as_paranoid
  #Validations
  validates :uuid, presence: true  #:pass_grade, #:num_questions, :gradable,
  validates :uuid, length: { in: 6..120 } #:created_by_uuid
 
  belongs_to :requests_category #, :dependent => :destroy
  has_many :questions, dependent: :destroy
  accepts_nested_attributes_for :questions, reject_if: lambda { |a| a[:content].blank? }, :allow_destroy => true

  #before
  before_validation :set_status, on: :create
  before_validation :load_uuid

  def self.scorecard_question_correct_answer(scorecard)
    Scorecard.find_by_sql("SELECT scorecards.pass_grade, questions.id as question_id, questions.multiple_answers,
                                  questions.score, answers.id as answer_id, answers.correct_answer, COALESCE(sub.cant, 0) AS quantity_correct
                           FROM `scorecards` 
                           INNER JOIN `questions` ON `questions`.`scorecard_id` = `scorecards`.`id` AND `questions`.`deleted_at` IS NULL 
                           INNER JOIN `answers` ON `answers`.`question_id` = `questions`.`id` AND `answers`.`deleted_at` IS NULL 
                           LEFT JOIN 
                                (SELECT questions.id as question_id, answers.correct_answer, count(*) AS cant 
                                     FROM `scorecards` 
                                     INNER JOIN `questions` ON `questions`.`scorecard_id` = `scorecards`.`id` AND `questions`.`deleted_at` IS NULL 
                                     INNER JOIN `answers` ON `answers`.`question_id` = `questions`.`id` AND `answers`.`deleted_at` IS NULL 
                                  WHERE `scorecards`.`deleted_at` IS NULL AND `scorecards`.`id` = #{scorecard} AND correct_answer = 1 
                                  GROUP BY question_id) AS sub 
                           ON sub.question_id = questions.id WHERE `scorecards`.`deleted_at` IS NULL AND `scorecards`.`id` = #{scorecard}")    
  end

  private
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
  def set_status
    @status  = StatusEntity.by_class('Scorecard', 'Active')
    self.status_entity_id = @status[0].id
  end
end