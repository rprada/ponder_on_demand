class Answer < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :question
  #accepts_nested_attributes_for :answers

  #Validations
  validates  :content, :uuid, presence: true #:score, :random_answer_order, presence: true
  validates :uuid, length: { in: 6..120 } #:created_by_uuid 
  #validates :correct_answer, :inclusion => { :in => [true, false] }

  
  #before
  before_validation :load_uuid
  private
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
end