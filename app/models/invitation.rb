class Invitation < ApplicationRecord
  acts_as_paranoid
  #self.primary_key = :email

  belongs_to :sent_by, class_name: 'User', foreign_key: 'sent_by_id'
  belongs_to :status_entity
  belongs_to :users_organization, foreign_key: 'sent_by_id'
  #belongs_to :sent_by, class_name: 'UsersOrganization', foreign_key: 'sent_by_id'
  #belongs_to :user, foreign_key: :email
  has_secure_token :invitation_token
  
#  belongs_to :sent_by, class_name: 'User', foreign_key: 'sent_by_id'


  validates :email, presence: true, format: { with: Devise.email_regexp }
  #validates :email, :sent_by, uniqueness: { scope: [:email, :sent_by] }
  #validate :presence_as_user, on: :create
  #enum status: [:pending, :accepted, :ignored]
  before_validation :load_uuid
  before_validation :set_status, on: :create
  after_create { Mailer.instructions(self).deliver_later }

  scope :by_user_organiz, ->(email, id) {
    joins(:users_organization).
    where("invitations.email = ? AND users_organizations.organization_id = ?", email, id )
  }

  scope :by_token, ->(token) {
    select("*, invitations.id, invitations.status_entity_id").
    joins(:users_organization).
    where("invitations.invitation_token = ?", token )
  }

  #scope :sent_by, -> (id) {
  #  joins(:status_entity, users_organization: :user).
  #  where("users_organizations.id = ?", id)
  #}
  def self.search(search)
    if search
      where('invitations.email LIKE ?', "%#{search}%").order('id DESC')
    else
      Invitation.all
    end
  end
  def self.get_by_users_organization id
    select("invitations.id, invitations.invitation_token, invitations.email, invitations.created_at,
            status_entities.name as status, invitations.sent_by_id, 
            invited.id AS user_id, invited.full_name, users.full_name as by_full_name")
    .joins(:status_entity, users_organization: :user)
    .joins("LEFT JOIN `users` AS invited ON invited.`email` = `invitations`.`email` AND  invited.`deleted_at` IS NULL")
    .where("users_organizations.id = ?", id)
  end

  private

  def presence_as_user
    errors.add(:email, :user_already_exists) if User.exists?(email: email )
  end
  def  load_uuid
    self.uuid = SecureRandom.uuid
  end
  def set_status
    @status  = StatusEntity.by_class('Invitation', 'Pending')
    self.status_entity_id = @status[0].id
  end
end