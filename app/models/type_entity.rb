class TypeEntity < ApplicationRecord
  acts_as_paranoid

  #Validations
  validates :uuid, :sort_name, :class_name,  presence: true #:created_by_uuid, presence: true
  #validates :uuid, :created_by_uuid , length: { in: 6..120 }
  validates :sort_name,  length: { in: 2..6 }
  #validates :created_by_uuid, :created_at, presence: true

  #Relationship

  belongs_to :status_entity

  #before
  before_validation :load_uuid
  private
  def  load_uuid
    self.uuid = SecureRandom.hex
  end
end
