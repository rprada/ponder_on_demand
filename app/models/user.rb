class User < ApplicationRecord
  acts_as_paranoid

  # Dev - Added for use of paperclip images
  # attr_accessible :avatar
  ROLES = %i[admin member-owner member]

  has_attached_file :avatar, :styles => { medium: "190x190#", thumb: "39x39#" }, default_url: "missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  #Validations
  validates  :full_name, :uuid, :first_name, :last_name, :email, :encrypted_password,
             :status_entity_id, presence: true
  validates :uuid, length: { in: 6..120 } #:created_by_uuid
  validates :uuid, :first_name , :last_name, length: { in: 2..100}
  #validates :email, email: true
  #Relationship

  belongs_to :status_entity #, :dependent => :destroy

  #belongs_to :organization, dependent: :destroy
  #has_and_belongs_to_many :organizations
  # has_many :invitations, foreign_key: 'sent_by_id'
  has_many :users_organizations
  has_many :organizations, through: :users_organizations
  has_many :invitations

  #has_many :invitations, foreign_key: 'sent_by_id'
  #has_many :organizations, through: :invitations

  #accepts_nested_attributes_for :invitations, reject_if: lambda { |a| a[:email].blank? }, :allow_destroy => true
  #accepts_nested_attributes_for :organizations
  accepts_nested_attributes_for :users_organizations




  #before
  before_validation :load_uuid
  before_validation :full_name_cache

  def self.current
    Thread.current[:user]
  end
  def self.current=(user)
    Thread.current[:user] = user
  end

  def self.searwdwdch(term, page)
    if term
      where('full_name LIKE ?', "%#{term}%").paginate(page: page, per_page: 5).order('id DESC')
    else
      paginate(page: page, per_page: 5).order('id DESC')
    end
  end

  private

    def  load_uuid
      self.uuid = SecureRandom.uuid
    end
    def full_name_cache
      self.full_name = [self.first_name, self.last_name].join(' ')
    end

end
