class RolesController < ApplicationController
  #  load_and_authorize_resource
    before_filter :authenticate_user!
    before_action :breadcrumb
    before_action :load_available, only: [:index, :edit]
    before_action :load_role, only: [:edit, :update, :destroy]

    def datatable_index
      render json: RoleDatatable.new(view_context)
    end

    def index
      @role = Role.new
    end
    def create
    	@role = Role.new(role_params)
      if @role.save
          render status: 200,
                   json: { success: true,
                           message: [ I18n.t('views_role.save_success'),
                                      I18n.t('views_role.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_role.save_error'), }
      end
    end
    def edit
      add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
      render 'index'
    end
    def update
      if @role.update_attributes(role_params)
         render status: 200,
                json: { success: true,
                        message: [ I18n.t('views_role.update_success'),
                                    I18n.t('views_role.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_role.update_error'), }
      end
    end
    def destroy
      if @role.destroy
          render status: 200,
                   json: { success: true,
                           message: [ I18n.t('views_role.delete_success'),
                                      I18n.t('views_role.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_role.delete_error'), }
      end
    end
    private
      def role_params
        params.require(:role).permit(:id, :status_entity_id, :name, :sort_name, :class_name, :description)
      end
      def breadcrumb
        add_breadcrumb I18n.t('views_role.breadcrumb'), :roles_path
      end
      def load_available
        @availableActive = StatusEntity.where(class_name:'role', name: 'Active', status: true)
        @availableInactive  = StatusEntity.where(class_name:'role', name: 'Inactive', status: true)

      end
      def load_role
        @role = Role.find(params[:id] )
      end
  end
