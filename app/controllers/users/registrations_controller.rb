class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]
  before_action :set_status, only: [:new]
  skip_before_action :set_current_user_organization, :only => [:new, :create, :checkEmail, :profile ]


  #prepend_before_action :require_no_authentication, only: [:new, :create, :cancel]
  #skip_before_action :authenticate_user!, only: [:new, :create, :cancel]
  #skip_before_filter :require_no_authentication, :only => [ :new, :create]
  # load_and_authorize_resource



  # GET /resource/sign_up
  def new
    resource  = build_resource({})
    users_org = resource.users_organizations.build
    organizat = users_org.build_organization
    users_org.invitations.build
    respond_with self.resource
  end

  def checkEmail
    @email = User.where(email: params[:user][:email])
    if @email.count == 0
        render json: {  valid: true }
    else
        render json: {  valid: false }
    end
  end

  def profile
    @user = User.find(params[:id])
    if @user.organization_id != nil
      @organization = Organization.find(@user.organization_id)
    end
    add_breadcrumb t('breadcrumbs.profile'), profile_path(current_user.id)
  end

  def update
    # required for settings form to submit when password is left blank
    if params[:user][:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
    end

    @user = User.find(current_user.id)
    if @user.update_attributes(user_params)
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      # sign_in @user, :bypass => true
      bypass_sign_in(@user)
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end

  # def update_resource(resource, params)
  #   resource.update_without_password(params)
  # end


  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    #super(resource)
    sign_out resource
    new_user_session_path
  end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  private

    def sign_up_params
      #allow = [:email, :your_fields, :password, :password_confirmation]
      params.require(resource_name).permit(:first_name, :second_last_name, :middle_name, :last_name, :organization_name, :email, :password, :password_confirmation, :status, :status_entity_id,                                   
                                           users_organizations_attributes: [ :id,
                                           organization_attributes: [:id, :name, :status_entity_id],
                                           invitations_attributes: [:id, :email, :status_entity_id]])
    end
    def set_status
      @statusUserAct   = StatusEntity.where(class_name:'User', name: 'Active', status: true)
      @statusUserInact = StatusEntity.where(class_name:'User', name: 'Inactive', status: true)
      @statusOrgAct    = StatusEntity.where(class_name:'Organization', name: 'Active', status: true)
    end

end