class Users::InvitationsController < Devise::InvitationsController
  respond_to :html, :json, :js
  
  before_action :set_status, only: [:create, :accept, :remove]
  before_action :set_invitation, only: [:remove]
  before_action :set_invitations, only: [:index, :remove]
  skip_before_action :set_current_user_organization, :only => [:accept, :create, :edit, :update]

  def index
  end
  def new
    self.resource = resource_class.new
    respond_modal_with @invitation
  end
  def create
    hashes = Array.new
    begin
      ActiveRecord::Base.transaction do
        params[:user][:emails].each do |email|
          exists = Invitation.by_user_organiz(email, UsersOrganization.current.organization_id)
          #puts exists.empty?
          unless !exists.present?
            hashes.push({ email: email, valid: false })
          else  
            hashes.push({ email: email, valid: true })
            Invitation.create!(email: email, 
                          sent_by_id: UsersOrganization.current.id)   
          end        
        end
      end
    rescue => e
      render json: { success: false, 
                     message: I18n.t('views_user.save_error'), }
    else
      #redirect_to index_invitations_path, notice: 'Invitations was successfully created.'
      render json: hashes
    end
  end
  def accept
    invitation =  Invitation.by_token(params[:token])
    exists     =  User.find_by(email: invitation[0].email)  
    if ((invitation.present?) && (invitation[0].status_entity_id === @statusPending[0].id))
      ActiveRecord::Base.transaction do    
        unless User.exists?(email: invitation[0].email )
          user = User.invite!(
            invitation_par(invitation[0], @statusActv[0].id).merge(
              skip_invitation: true
            ),
            invitation[0].sent_by
          )
          create_users_organization user, invitation
          redirect_to accept_invitation_url(user, invitation_token: user.raw_invitation_token)      
        else
          create_users_organization exists, invitation
          redirect_to authenticated_root_path, notice: 'Users organization was successfully created.'
        end
      end
    else        
      flash[:error] = t('en.invitations.accept.not_valid')
      redirect_to authenticated_root_path
    end
  end
  def resend
    invitation = Invitation.find_by(invitation_token: params[:token])
    Mailer.instructions(invitation).deliver_later  
    render json: { success: true }
  end
  def update
    super
  end
  def remove
    unless @invitation.status_entity_id == @statusAccepted[0].id
      if @invitation.destroy
        @success = true
        flash.now[:notice] = "Here is my flash notice" 
      else
        flash.now[:error] = "Here is my flash notice" 
      end
    else
      flash.now[:error] = "NO notice"
    end
    #respond_to do |f|
    #  f.js  
    #end
  end

  private

  def breadcrumb
    #add_breadcrumb I18n.t('views_company.breadcrumb'), :admin_companies_path
  end
  def set_status
    @statusAccepted = StatusEntity.where(class_name:'Invitation', name: 'Accepted', status: true)
    @statusPending  = StatusEntity.where(class_name:'Invitation', name: 'Pending', status: true)
    @statusRejected = StatusEntity.where(class_name:'Invitation', name: 'Rejected', status: true)
    @statusActv     = StatusEntity.where(class_name:'User', name: 'Active', status: true)
  end
  def set_invitation
    @invitation = Invitation.find(params[:id])
  end
  def invitation_par invitation, status
    { email: invitation.email, status_entity_id: status }
  end
  def set_invitations
    @invitations = Invitation.get_by_users_organization(UsersOrganization.current.id)
                             .search(params[:search])
                             .paginate(:per_page => 8, :page => params[:page])    
  end
  def create_users_organization user, invitation
    users_organization  =  UsersOrganization.create!(user_id: user.id, 
                                             organization_id: invitation[0].organization_id,
                                      users_organization_id:  invitation[0].sent_by_id)
    users_org_role      =  users_organization.build_users_organizations_role # = users_organization
    users_org_role.role =  Role.where(name: 'Member').first
    users_org_role.save
    user.update(invitation_sent_at: Time.now.utc)
    invitation.update(status_entity_id: @statusAccepted[0].id)
  end

  protected
  def invite_resource(&block)
    @user = User.find_by(email: invite_params[:email])
    # @user is an instance or nil
    if @user && @user.email != current_user.email
      # invite! instance method returns a Mail::Message instance
      @user.invite!(current_user)
      # return the user instance to match expected return type
      @user
    else
      # invite! class method returns invitable var, which is a User instance
      resource_class.invite!(invite_params, current_inviter, &block)
    end
  end
end