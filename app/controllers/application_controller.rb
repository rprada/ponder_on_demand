class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  #before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale
  before_action :set_current_user
  before_action :set_current_user_organization

  $question_id_global = ""

  def default_url_options
    {locale: I18n.locale}
  end
  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end

  def set_current_user
    User.current = current_user
  end

  def set_current_user_organization
    UsersOrganization.current = UsersOrganization.by_user(current_user)
  end

  protected

  def configure_permitted_parameters
    #devise_parameter_sanitizer.permit(:sign_up, keys: [:avatar, :first_name, :second_last_name, :middle_name, :last_name, :organization_name, :email, :password, :password_confirmation, :status, :status_entity_id])
    # devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
    devise_parameter_sanitizer.permit(:account_update, keys: [:avatar, :first_name, :second_last_name, :middle_name, :last_name, :organization_name, :email, :password, :password_confirmation, :status, :status_entity_id, :current_password])
    devise_parameter_sanitizer.permit(:accept_invitation, keys: [:first_name, :last_name, :status_entity_id])
    devise_parameter_sanitizer.permit(:invite, keys: [:status_entity_id])

    #devise_parameter_sanitizer.permit(:sign_up, keys: [:avatar, :first_name, :second_last_name, :middle_name, :last_name, :organization_name, :email, :password, :password_confirmation, :status, :status_entity_id, :role])
    # devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
    devise_parameter_sanitizer.permit(:account_update, keys: [:avatar, :first_name, :second_last_name, :middle_name, :last_name, :organization_name, :email, :password, :password_confirmation, :status, :status_entity_id, :current_password, :role])
  end
  def set_locale
    I18n.locale = current_user.try(:locale) || params[:locale] || I18n.default_locale
  end
end
