class ScorecardsController < ApplicationController
  before_action :set_scorecard, only: [:show, :edit, :update, :destroy]


  before_action :set_avalable, only: [:index, :edit, :onlyQuestionsAnswers, :removeQuestionsAnswers, :update]
  before_action :set_type_entity, only: [:index, :edit, :new]
  before_action :set_question_type, only: [:index, :edit, :new]
  before_action :set_request, only: [:onlyQuestionsAnswers, :removeQuestionsAnswers, :update]
  before_action :set_scorecard,  only: [:edit, :update, :destroy, :onlyQuestionsAnswers, :removeQuestionsAnswers]
 # before_action :breadcrumb


  # GET /scorecards
  def index
    @scorecard = Scorecard.new
    respond_to do |format|
        format.html
        format.js #-> loads /views/cookbooks/index.js.erb
    end
  end
  # GET /scorecards/new
  def new
    @scorecard = Scorecard.new
  end
  def onlyQuestionsAnswers
    has = Hash[questions_params.map {|k,v| [k,(v.respond_to?(:except)?v.extract!(params[:id2]):v)] }]
    if @scorecard.update_attributes!(has)
       render status: 200,
                json: { success: true,
                             id: $question_id_global,
                         object: @scorecard.questions.get_answers($question_id_global),
                        message: [ I18n.t('views_request_category.scorecard.questions.save_success'),
                                   I18n.t('views_request_category.success')]}
       #@request.each_with_index do  |e, i|
       # RequestScorecard.where(request_id: e.id).destroy_all
       #end
    else
      render html: :new      
    end    
  end
  def removeQuestionsAnswers    
    has = Hash[questions_params.map {|k,v| [k,(v.respond_to?(:except)?v.extract!(params[:id2]):v)] }]
    #hash =  change_destroy(has, params[:id2])
    has["questions_attributes"][params[:id2]][:_destroy] = '1'
    if @scorecard.update_attributes!(has)
      render  status: 200,
                json: { success: true,
                         object: @questions,
                        message: [ I18n.t('views_request_category.scorecard.questions.delete_success'),
                                    I18n.t('views_request_category.success')]}
      # @request.each_with_index do  |e, i|
      #  RequestScorecard.where(request_id: e.id).destroy_all
      # end
    else
      render html: :new      
    end       
  end
  def update
    if @scorecard.update_attributes(scorecard_params)
      render status: 200,
               json: { success: true,
                       message: [ I18n.t('views_request_category.scorecard.update_success'),
                                I18n.t('views_request_category.scorecard.success')]}
      #@request.each_with_index do  |e, i|
      #  RequestScorecard.where(request_id: e.id).update_all(gradable: @scorecard.gradable, pass_grade: @scorecard.pass_grade )
        #RequestScorecard.where(request_id: e.id).destroy_all
      #end
    else
      render html: :new
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_scorecard
      @scorecard = Scorecard.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def questions_params
      params.require(:scorecard).permit(:id, 
          questions_attributes:[:id, :content, :score, :random_answer_order, :type_entity_id, :multiple_answers, :_destroy,
          answers_attributes:[:id, :content, :score, :correct_answer, :_destroy]])
    end
    def scorecard_params
      params.require(:scorecard).permit(:id, :gradable, :pass_grade, :different_question_order, :num_questions, :random_questions_order, :type_entity_id, :status_entity_id, :request_category_id, :type_different_question )
    end
    def breadcrumb
      add_breadcrumb I18n.t('views_request_category.breadcrumb'), :admin_request_categories_path
    end
    def change_destroy(hash, pa)
      hash.each do |key,v|
        if key == "questions_attributes"
          v[pa].to_h[:_destroy] = '1'
        end
      end        
    end
    def set_avalable
      @avalableActive   = StatusEntity.where(class_name:'Scorecard', name: 'Active',   status: true)
      @avalableInactive = StatusEntity.where(class_name:'Scorecard', name: 'Inactive', status: true)
      @statusNew   = StatusEntity.where(class_name:'Request', name: 'New', status: true)
    end
    def set_request
    #  @request = Request.get_scorecard_status_new(params[:id],@statusNew[0].id)      
    end
    def set_type_entity
      @entityActive   = StatusEntity.where(class_name:'TypeEntity', name: 'Active',   status: true)
      @entityInactive = StatusEntity.where(class_name:'TypeEntity', name: 'Inactive', status: true)
    end
    def set_question_type
      @yes_no  = TypeEntity.where(class_name:'Question', name: 'Yes/No', status_entity_id: @entityActive[0].id )
      @simple  = TypeEntity.where(class_name:'Question', name: 'Simple', status_entity_id: @entityActive[0].id )
      @combo   = TypeEntity.where(class_name:'Question', name: 'Combo' , status_entity_id: @entityActive[0].id  )
      @multiple_options = TypeEntity.where(class_name:'Question', name: 'Multiple Options', status_entity_id: @entityActive[0].id )
    end
end
