class CountiesController < ApplicationController
  # load_and_authorize_resource
   before_filter :authenticate_user!
   before_action :breadcrumb
   before_action :load_available, only: [:index, :edit]
   before_action :load_county, only: [:edit, :update, :destroy]

   def datatable_index
     render json: CountyDatatable.new(view_context)
   end

   def index
     @county = County.new
   end
   def create
     @county = County.new(county_params)
     @county.user_id = current_user.id
     if @county.save
         render status: 200,
                  json: { success: true,
                          message: [ I18n.t('views_county.save_success'),
                                     I18n.t('views_county.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_county.save_error'), }
     end
   end
   def edit
     add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
     render 'index'
   end
   def update
     if @county.update_attributes(county_params)
        render status: 200,
               json: { success: true,
                       message: [ I18n.t('views_county.update_success'),
                                   I18n.t('views_county.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_county.update_error'), }
     end
   end
   def destroy
     if @county.destroy
         render status: 200,
                  json: { success: true,
                          message: [ I18n.t('views_county.delete_success'),
                                     I18n.t('views_county.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_county.delete_error'), }
     end
   end
   private
   def county_params
     # params.permit(:name, :sort_name, :class_name, :description, :status_entity_id, :id)
     params.require(:county).permit(:id, :sort_name, :state_id, :name, :description, :status_entity_id, :user_id )
   end
   def breadcrumb
     add_breadcrumb I18n.t('views_county.breadcrumb'), :type_entities_path
   end
   def load_available
     @availableActive = StatusEntity.where(class_name:'county', name: 'Active', status: true)
     @availableInactive  = StatusEntity.where(class_name:'county', name: 'Inactive', status: true)
   end
   def load_county
     @county = County.find(params[:id] )
   end
end
