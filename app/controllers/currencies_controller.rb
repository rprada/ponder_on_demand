class CurrenciesController < ApplicationController

  respond_to :html, :json, :js
  before_action :breadcrumb
  before_action :set_avalable, only: [:index, :edit]
  before_action :set_currency, only: [:edit, :update, :destroy]

  def datatable_index
    render json: CurrencyDatatable.new(view_context)
  end
  def index
    @currency = Currency.new
  end
  def create
    @currency = Currency.new(currency_params)
    respond_to do |format|
      if @currency.save
        format.html { redirect_to currencies_url, notice: 'Currency was successfully created.' }
        format.json { render :show, status: :created, location: @currency }
      else
        format.html { render :new }
        #format.json { render json: @currency.errors, status: :unprocessable_entity }
      end
    end
  end
  def edit
    add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
    render 'index'
  end
  def update
    if @currency.update(currency_params)
      @success = true
      flash.now[:notice] = "Currency was successfully updated." 
    else
      flash.now[:error] = "Currency updated." 
    end
  end
  def destroy
    if @currency.destroy
      @success = true
      flash.now[:notice] = "Currency was successfully destroy." 
    else
      flash.now[:error] = "Currency error destroy." 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_currency
      @currency = Currency.find(params[:id])
    end

    def currency_params
      params.require(:currency).permit(:id, :name, :sort_name, :symbol, :status_entity_id, country_ids:[])
    end
    def breadcrumb
      add_breadcrumb I18n.t('views_currency.breadcrumb'), :currencies_path
    end
    def set_avalable
      @avalableActive    = StatusEntity.where(class_name:'Currency', name: 'Active', status: true)
      @avalableInactive  = StatusEntity.where(class_name:'Currency', name: 'Inactive', status: true)
    end
end
