class UsersOrganizationsController < ApplicationController
  
  before_action :load_available, only: [:index, :edit]    
  before_action :set_users_organization, only: [:show, :edit, :update, :destroy]

  def datatable_index
    render json: UsersOrganizationDatatable.new(view_context)
  end
  # GET /users_organizations/index
  def index
    @users_organization = UsersOrganization.new
    @users_organization.build_user
    @users_organization.build_organization
    @users_organizations_role = @users_organization.build_users_organizations_role
  end
  # GET /users_organizations/1/edit
  def edit
    add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
    @users_organizations_role = @users_organization.users_organizations_role
    users_org = UsersOrganization.managed_by(@users_organization.id)
    @data = users_org.collect { |key| { 'value': key[:id], 'text': key[:full_name] }}
    @data_json = @data.to_json 
    render 'index'
  end
  def create_member user, organization
    
  end
  # PATCH/PUT /users_organizations/1
  def update
    respond_to do |format|
      begin
        ActiveRecord::Base.transaction do
          if @users_organization.update(users_organization_params)
            array = users_organization_params[:managed_by].tr('[]', '').split(',').map(&:to_i)
            unless array.blank?                
              array.each do |user|
                #unless user.to_i === 0              
                users_organization =  UsersOrganization.find_by(user)          
                users_organization.update_attributes!( users_organization_id: @users_organization.id )
                #end
              end
            end
          end
        end
      rescue => e
        format.js {  flash.now[:notice] = "ERROR"  }
      else
        format.js {  flash.now[:notice] = "Here is my flash notice"  }
      end
    end
  end
  def data_json
    @users_org = UsersOrganization.by_organiz(UsersOrganization.current.organization_id)
    if @users_org
      @data = @users_org.collect {|key| { 'value': key[:id], 'text': key[:full_name] }}
      render :json => @data.to_json 
    end  
  end
  def org_tree
    data = UsersOrganization
           .select("users_organizations.id, users.full_name as name, users_organizations.users_organization_id")
           .joins(:user)
           .where(organization_id: UsersOrganization.current.organization_id)
    hash = ActiveSupport::JSON.encode(data)
    json =  JSON.parse(hash)
    nested_hash = Hash[json.map{|e| [e['id'], e.merge(children: [])]}]
    nested_hash.each do |id, item|
      parent = nested_hash[item['users_organization_id']]
      parent[:children] << item if parent
    end
    tree = nested_hash.select { |id, item| item['users_organization_id'].nil? }.values
    fin = ActiveSupport::JSON.encode(tree)
    ar = fin.chop
    art = ar[1..ar.length]
    render :json => art    
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_organization
      @users_organization = UsersOrganization.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def users_organization_params
      params.require(:users_organization).permit(:id, :job_title, :managed_by, :users_organization_id, :status_entity_id,  users_organizations_role_attributes: [:id, :role_id])
    end
    def load_available
      @availableActive    = StatusEntity.where(class_name:'UsersOrganization', name: 'Active', status: true)
      @availableInactive  = StatusEntity.where(class_name:'UsersOrganization', name: 'Inactive', status: true)
    end
end