class OrganizationsController < ApplicationController
 # load_and_authorize_resource
  before_filter :authenticate_user!
  before_action :breadcrumb
  before_action :load_available, only: [:index, :edit]
  before_action :load_organization, only: [:edit, :update, :destroy]

  def datatable_index
    render json: OrganizationDatatable.new(view_context)
  end

  def index
    @organization = Organization.new
  end
  def create
  	@organization = Organization.new(organization_params)
    if @organization.save
        render status: 200,
                 json: { success: true,
                         message: [ I18n.t('views_organization.save_success'),
                                    I18n.t('views_organization.success')]}
    else
      render json: { success: false,
                     message: I18n.t('views_organization.save_error'), }
    end
  end
  def edit
    add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
    render 'index'
  end
  def update
    if @organization.update_attributes(organization_params)
       render status: 200,
              json: { success: true,
                      message: [ I18n.t('views_organization.update_success'),
                                  I18n.t('views_organization.success')]}
    else
      render json: { success: false,
                     message: I18n.t('views_organization.update_error'), }
    end
  end
  def destroy
    if @organization.destroy
        render status: 200,
                 json: { success: true,
                         message: [ I18n.t('views_organization.delete_success'),
                                    I18n.t('views_organization.success')]}
    else
      render json: { success: false,
                     message: I18n.t('views_organization.delete_error'), }
    end
  end

  private
    def organization_params
      params.require(:organization).permit(:id, :status_entity_id, :name, :website, :phone_number, :address)
    end
    def breadcrumb
      add_breadcrumb I18n.t('views_organization.breadcrumb'), :organizations_path
    end
    def load_available
      @availableActive = StatusEntity.where(class_name:'organization', name: 'Active', status: true)
      @availableInactive  = StatusEntity.where(class_name:'organization', name: 'Inactive', status: true)
    end
    def load_organization
      @organization = Organization.find(params[:id] )
    end
end
