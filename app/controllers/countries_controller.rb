class CountriesController < ApplicationController
  # load_and_authorize_resource
   before_filter :authenticate_user!
   before_action :breadcrumb
   before_action :load_available, only: [:index, :edit]
   before_action :load_country, only: [:edit, :update, :destroy]

   def datatable_index
     render json: CountryDatatable.new(view_context)
   end

   def index
     @country = Country.new
   end
   def create
     @country = Country.new(country_params)
     @country.user_id = current_user.id
     if @country.save
         render status: 200,
                  json: { success: true,
                          message: [ I18n.t('views_country.save_success'),
                                     I18n.t('views_country.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_country.save_error'), }
     end
   end
   def edit
     add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
     render 'index'
   end
   def update
     if @country.update_attributes(country_params)
        render status: 200,
               json: { success: true,
                       message: [ I18n.t('views_country.update_success'),
                                   I18n.t('views_country.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_country.update_error'), }
     end
   end
   def destroy
     if @country.destroy
         render status: 200,
                  json: { success: true,
                          message: [ I18n.t('views_country.delete_success'),
                                     I18n.t('views_country.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_country.delete_error'), }
     end
   end
   private
   def country_params
     params.require(:country).permit(:id, :sort_name, :name, :language, :description, :status_entity_id, :user_id )
   end
   def breadcrumb
     add_breadcrumb I18n.t('views_country.breadcrumb'), :countries_path
   end
   def load_available
     @availableActive = StatusEntity.where(class_name:'country', name: 'Active', status: true)
     @availableInactive  = StatusEntity.where(class_name:'country', name: 'Inactive', status: true)
   end
   def load_country
     @country = Country.find(params[:id] )
   end
end
