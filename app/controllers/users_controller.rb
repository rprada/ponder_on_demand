class UsersController < ApplicationController
    #load_and_authorize_resource
    before_filter :authenticate_user!
    before_action :breadcrumb
    before_action :load_available, only: [:index, :edit]
    before_action :load_user, only: [:edit, :update, :destroy]

    def datatable_index
      render json: UserDatatable.new(view_context)
    end

    def index
      @user = User.new
    end
    def create
    	@user = User.new(user_params)
      if @user.save
          render status: 200,
                   json: { success: true,
                           message: [ I18n.t('views_user.save_success'),
                                      I18n.t('views_user.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_user.save_error'), }
      end
    end
    def edit
      add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
      render 'index'
    end
    def update
      if @user.update_attributes(user_params)
         render status: 200,
                json: { success: true,
                        message: [ I18n.t('views_user.update_success'),
                                    I18n.t('views_user.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_user.update_error'), }
      end
    end
    def destroy
      if @user.destroy
          render status: 200,
                   json: { success: true,
                           message: [ I18n.t('views_user.delete_success'),
                                      I18n.t('views_user.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_user.delete_error'), }
      end
    end
    private
      def user_params
        params.require(:user).permit(:id, :first_name, :second_last_name, :middle_name, :last_name, :email, :password, :password_confirmation, :status, :status_entity_id, :role)

      end
      def breadcrumb
        add_breadcrumb I18n.t('views_user.breadcrumb'), :users_path
      end
      def load_available
        @availableActive = StatusEntity.where(class_name:'user', name: 'Active', status: true)
        @availableInactive  = StatusEntity.where(class_name:'user', name: 'Inactive', status: true)
      end
      def load_user
        @user = User.find(params[:id] )
      end

  end
