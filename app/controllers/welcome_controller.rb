class WelcomeController < ApplicationController
  # load_and_authorize_resource
  #layout 'bb8'
  #before_filter :authenticate_user!
  def index
    add_breadcrumb t('breadcrumbs.welcome'), authenticated_root_path
  end
end
