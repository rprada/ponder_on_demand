class StatesController < ApplicationController
  # load_and_authorize_resource
   before_filter :authenticate_user!
   before_action :breadcrumb
   before_action :load_available, only: [:index, :edit]
   before_action :load_state, only: [:edit, :update, :destroy]

   def datatable_index
     render json: StateDatatable.new(view_context)
   end

   def index
     @state = State.new
   end
   def create
     @state = State.new(state_params)
     @state.user_id = current_user.id
     if @state.save
         render status: 200,
                  json: { success: true,
                          message: [ I18n.t('views_state.save_success'),
                                     I18n.t('views_state.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_state.save_error'), }
     end
   end
   def edit
     add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
     render 'index'
   end
   def update
     if @state.update_attributes(state_params)
        render status: 200,
               json: { success: true,
                       message: [ I18n.t('views_state.update_success'),
                                   I18n.t('views_state.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_state.update_error'), }
     end
   end
   def destroy
     if @state.destroy
         render status: 200,
                  json: { success: true,
                          message: [ I18n.t('views_state.delete_success'),
                                     I18n.t('views_state.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_state.delete_error'), }
     end
   end
   def get_counties_by_state
      counties = County.where(state_id: params[:state_id]).order('name ASC').select(:id, :name)
      respond_to do |format|
        format.json { render json: counties  }
      end    
   end
   private
   def state_params
     # params.permit(:name, :sort_name, :class_name, :description, :status_entity_id, :id)
     params.require(:state).permit(:id, :sort_name, :country_id, :name, :time_zone, :description, :status_entity_id )
   end
   def breadcrumb
     add_breadcrumb I18n.t('views_state.breadcrumb'), :type_entities_path
   end
   def load_available
     @availableActive = StatusEntity.where(class_name:'state', name: 'Active', status: true)
     @availableInactive  = StatusEntity.where(class_name:'state', name: 'Inactive', status: true)
   end
   def load_state
     @state = State.find(params[:id] )
   end
end
