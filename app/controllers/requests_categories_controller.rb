class RequestsCategoriesController < ApplicationController
  before_action :load_available, only: [:index, :edit]    
  before_action :load_type_entity, only: [:index, :edit]    
  before_action :set_requests_category, only: [:show, :edit, :update, :destroy]

  def datatable_index
    render json: RequestsCategoryDatatable.new(view_context)
  end
  # GET /requests_categories
  def index
    @requests_category =  RequestsCategory.new
    @scorecard         =  @requests_category.build_scorecard
  end
  # GET /requests_categories/1/edit
  def edit
    render 'index'
  end
  # POST /requests_categories
  def create
    @requests_category = RequestsCategory.new(requests_category_params)
    respond_to do |format|
      if @requests_category.save
        format.html { redirect_to @requests_category, notice: 'Requests category was successfully created.' }
        format.json { render :show, status: :created, location: @requests_category }
      else
        format.html { render :new }
        format.json { render json: @requests_category.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /requests_categories/1
  def update
    respond_to do |format|
      if @requests_category.update(requests_category_params)
        format.html { redirect_to requests_categories_url , notice: 'Requests category was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /requests_categories/1
  def destroy
    @requests_category.destroy
    respond_to do |format|
      format.html { redirect_to requests_categories_url, notice: 'Requests category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_requests_category
      @requests_category = RequestsCategory.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def requests_category_params
      params.require(:requests_category).permit(:id, :name, :status_entity_id, :scorecard, :type_entity_id, :organization_id,
                                                scorecard_attributes: [:id])
    end
    def load_available
      @availableActive    = StatusEntity.where(class_name:'RequestsCategory', name: 'Active',   status: true)
      @availableInactive  = StatusEntity.where(class_name:'RequestsCategory', name: 'Inactive', status: true)
    end
    def load_type_entity
      @typePublic   =  TypeEntity.where(class_name:'RequestsCategory', name: 'Public')
      @typePrivate  =  TypeEntity.where(class_name:'RequestsCategory', name: 'Private')
    end
end
