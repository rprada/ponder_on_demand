class TypeEntitiesController < ApplicationController
   # load_and_authorize_resource
    before_filter :authenticate_user!
    before_action :breadcrumb
    before_action :load_available, only: [:index, :edit]
    before_action :load_type_entity, only: [:edit, :update, :destroy]

    def datatable_index
      render json: TypeEntityDatatable.new(view_context)
    end

    def index
      @type_entity = TypeEntity.new
    end
    def create
    	@type_entity = TypeEntity.new(type_entity_params)
      if @type_entity.save
          render status: 200,
                   json: { success: true,
                           message: [ I18n.t('views_type_entity.save_success'),
                                      I18n.t('views_type_entity.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_type_entity.save_error'), }
      end
    end
    def edit
      add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
      render 'index'
    end
    def update
      if @type_entity.update_attributes(type_entity_params)
         render status: 200,
                json: { success: true,
                        message: [ I18n.t('views_type_entity.update_success'),
                                    I18n.t('views_type_entity.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_type_entity.update_error'), }
      end
    end
    def destroy
      if @type_entity.destroy
          render status: 200,
                   json: { success: true,
                           message: [ I18n.t('views_type_entity.delete_success'),
                                      I18n.t('views_type_entity.success')]}
      else
        render json: { success: false,
                       message: I18n.t('views_type_entity.delete_error'), }
      end
    end
    private
    def type_entity_params
      # params.permit(:name, :sort_name, :class_name, :description, :status_entity_id, :id)
      params.require(:type_entity).permit(:id, :sort_name, :class_name, :name, :description, :status_entity_id )
    end
    def breadcrumb
      add_breadcrumb I18n.t('views_type_entity.breadcrumb'), :type_entities_path
    end
    def load_available
      @availableActive = StatusEntity.where(class_name:'TypeEntity', name: 'Active', status: true)
      @availableInactive  = StatusEntity.where(class_name:'TypeEntity', name: 'Inactive', status: true)
    end
    def load_type_entity
      @type_entity = TypeEntity.find(params[:id] )
    end

  end
