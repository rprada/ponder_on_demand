class UsersOrganizationsRolesController < ApplicationController
  before_action :set_users_organizations_role, only: [:show, :edit, :update, :destroy]


  # GET /users_organizations_roles
  # GET /users_organizations_roles.json
    def index
    @users_organizations_roles = UsersOrganizationsRole.all
  end

  # GET /users_organizations_roles/1
  # GET /users_organizations_roles/1.json
  def show
  end

  # GET /users_organizations_roles/new
  def new
    @users_organizations_role = UsersOrganizationsRole.new
  end

  # GET /users_organizations_roles/1/edit
  def edit
  end

  # POST /users_organizations_roles
  # POST /users_organizations_roles.json
  def create
    @users_organizations_role = UsersOrganizationsRole.new(users_organizations_role_params)

    respond_to do |format|
      if @users_organizations_role.save
        format.html { redirect_to @users_organizations_role, notice: 'Users organizations role was successfully created.' }
        format.json { render :show, status: :created, location: @users_organizations_role }
      else
        format.html { render :new }
        format.json { render json: @users_organizations_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_organizations_roles/1
  # PATCH/PUT /users_organizations_roles/1.json
  def update
    respond_to do |format|
      if @users_organizations_role.update(users_organizations_role_params)
        format.html { redirect_to @users_organizations_role, notice: 'Users organizations role was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_organizations_role }
      else
        format.html { render :edit }
        format.json { render json: @users_organizations_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_organizations_roles/1
  # DELETE /users_organizations_roles/1.json
  def destroy
    @users_organizations_role.destroy
    respond_to do |format|
      format.html { redirect_to users_organizations_roles_url, notice: 'Users organizations role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_organizations_role
      @users_organizations_role = UsersOrganizationsRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_organizations_role_params
      params.require(:users_organizations_role).permit(:users_organization, :role_id)
    end
end
