class CitiesController < ApplicationController
  # load_and_authorize_resource
   before_filter :authenticate_user!
   before_action :breadcrumb
   before_action :load_available, only: [:index, :edit]
   before_action :load_city, only: [:edit, :update, :destroy]

   def datatable_index
     render json: CityDatatable.new(view_context)
   end

   def index
     @city = City.new
     if params[:filter].blank?
       @counties = County.all
     else
       @counties = County.where(state_id: params[:filter])
      #  render js: "alert('sdd');$(\"[name='city[county_id]'\").html(\"#{ j select 'counties', 'name', @counties.collect {|c| [c.name, c.id]}}\");"
      #  render js: "alert('sdd');$(\"[name='city[county_id]'\").html(\"#&{ j select 'citiesttt', 'county_id', @counties.collect {|p| [p.name, p.id]}}\");"
      #  $(\"[name='city[county_id]'\").html(\"#{ j select 'cities', 'name', @counties.collect {|p| [p.name, p.id]}}\");
     end
   end
   def create
     @city = City.new(city_params)
     @city.user_id = current_user.id
     if @city.save
         render status: 200,
                  json: { success: true,
                          message: [ I18n.t('views_city.save_success'),
                                     I18n.t('views_city.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_city.save_error'), }
     end
   end
   def edit
     add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
     render 'index'
   end
   def update
     if @city.update_attributes(city_params)
        render status: 200,
               json: { success: true,
                       message: [ I18n.t('views_city.update_success'),
                                   I18n.t('views_city.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_city.update_error'), }
     end
   end
   def destroy
     if @city.destroy
         render status: 200,
                  json: { success: true,
                          message: [ I18n.t('views_city.delete_success'),
                                     I18n.t('views_city.success')]}
     else
       render json: { success: false,
                      message: I18n.t('views_city.delete_error'), }
     end
   end
   private
   def city_params
     params.require(:city).permit(:id, :sort_name, :state_id, :name, :description, :status_entity_id, :county_id, :user_id )
   end
   def breadcrumb
     add_breadcrumb I18n.t('views_city.breadcrumb'), :type_entities_path
   end
   def load_available
     @availableActive = StatusEntity.where(class_name:'city', name: 'Active', status: true)
     @availableInactive  = StatusEntity.where(class_name:'city', name: 'Inactive', status: true)
   end
   def load_city
     @city = City.find(params[:id] )
   end
end
