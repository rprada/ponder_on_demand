class StatusEntitiesController < ApplicationController
  #load_and_authorize_resource
  before_filter :authenticate_user!
  before_action :breadcrumb
  before_action :load_available, only: [:index, :edit]
  before_action :load_status_entity, only: [:edit, :update, :destroy]

  def datatable_index
    render json: StatusEntityDatatable.new(view_context)
  end

  def index
    @status_entity = StatusEntity.new
  end
  def create
  	@status_entity = StatusEntity.new(status_entity_params)
    if @status_entity.save
        render status: 200,
                 json: { success: true,
                         message: [ I18n.t('views_status_entity.save_success'),
                                    I18n.t('views_status_entity.success')]}
    else
      render json: { success: false,
                     message: I18n.t('views_status_entity.save_error'), }
    end
  end
  def edit
    add_breadcrumb params[:locale] == 'en' ? "edit" : "editar"
    render 'index'
  end
  def update
    if @status_entity.update_attributes(status_entity_params)
       render status: 200,
              json: { success: true,
                      message: [ I18n.t('views_status_entity.update_success'),
                                  I18n.t('views_status_entity.success')]}
    else
      render json: { success: false,
                     message: I18n.t('views_status_entity.update_error'), }
    end
  end
  def destroy
    if @status_entity.destroy
        render status: 200,
                 json: { success: true,
                         message: [ I18n.t('views_status_entity.delete_success'),
                                    I18n.t('views_status_entity.success')]}
    else
      render json: { success: false,
                     message: I18n.t('views_status_entity.delete_error'), }
    end
  end
  private
    def status_entity_params
      params.require(:status_entity).permit(:id, :sort_name, :name, :class_name, :status )
    end
    def breadcrumb
      add_breadcrumb I18n.t('views_status_entity.breadcrumb'), :status_entities_path
    end
    def load_available
      @availableYes = StatusEntity.where(class_name:'status_entity', name: 'Yes', status: true)
      @availableNo  = StatusEntity.where(class_name:'status_entity', name: 'No', status: true)
    end
    def load_status_entity
      @status_entity = StatusEntity.find(params[:id] )
    end

end
