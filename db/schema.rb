# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170828024515) do

  create_table "answers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",           limit: 129,   default: "", null: false
    t.text     "content",        limit: 65535,              null: false
    t.integer  "score"
    t.boolean  "correct_answer"
    t.integer  "question_id"
    t.datetime "deleted_at"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["deleted_at"], name: "index_answers_on_deleted_at", using: :btree
    t.index ["question_id"], name: "index_answers_on_question_id", using: :btree
  end

  create_table "cities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 200,   default: "", null: false
    t.integer  "status_entity_id",                            null: false
    t.integer  "state_id"
    t.integer  "user_id",                                     null: false
    t.integer  "county_id"
    t.string   "sort_name",        limit: 6,     default: "", null: false
    t.string   "name",             limit: 250,   default: "", null: false
    t.text     "description",      limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.index ["deleted_at"], name: "index_cities_on_deleted_at", using: :btree
    t.index ["state_id"], name: "index_cities_on_state_id", using: :btree
    t.index ["status_entity_id"], name: "index_cities_on_status_entity_id", using: :btree
    t.index ["user_id"], name: "index_cities_on_user_id", using: :btree
  end

  create_table "counties", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 129,   default: "", null: false
    t.integer  "status_entity_id",                            null: false
    t.integer  "state_id",                                    null: false
    t.integer  "user_id",                                     null: false
    t.string   "sort_name",        limit: 6,                  null: false
    t.string   "name",             limit: 250,   default: "", null: false
    t.text     "description",      limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.index ["deleted_at"], name: "index_counties_on_deleted_at", using: :btree
    t.index ["state_id"], name: "index_counties_on_state_id", using: :btree
    t.index ["status_entity_id"], name: "index_counties_on_status_entity_id", using: :btree
    t.index ["user_id"], name: "index_counties_on_user_id", using: :btree
  end

  create_table "countries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 129,   default: "", null: false
    t.integer  "status_entity_id",                            null: false
    t.integer  "user_id",                                     null: false
    t.string   "sort_name",        limit: 6,                  null: false
    t.string   "name",             limit: 250,   default: "", null: false
    t.string   "language",         limit: 100,   default: "", null: false
    t.text     "description",      limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.index ["deleted_at"], name: "index_countries_on_deleted_at", unique: true, using: :btree
    t.index ["status_entity_id"], name: "index_countries_on_status_entity_id", using: :btree
    t.index ["user_id"], name: "index_countries_on_user_id", using: :btree
  end

  create_table "countries_currencies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "country_id",  null: false
    t.integer  "currency_id", null: false
    t.datetime "deleted_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["country_id"], name: "index_countries_currencies_on_country_id", using: :btree
    t.index ["currency_id"], name: "index_countries_currencies_on_currency_id", using: :btree
    t.index ["deleted_at"], name: "index_countries_currencies_on_deleted_at", using: :btree
  end

  create_table "currencies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 200, default: "", null: false
    t.integer  "status_entity_id",                          null: false
    t.string   "sort_name",        limit: 10,  default: "", null: false
    t.string   "name",             limit: 250, default: "", null: false
    t.string   "symbol",           limit: 250, default: "", null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["deleted_at"], name: "index_currencies_on_deleted_at", using: :btree
    t.index ["status_entity_id"], name: "index_currencies_on_status_entity_id", using: :btree
  end

  create_table "invitations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 129, default: "", null: false
    t.integer  "sent_by_id",                                null: false
    t.string   "email",                                     null: false
    t.string   "invitation_token",                          null: false
    t.integer  "status_entity_id",                          null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["deleted_at"], name: "index_invitations_on_deleted_at", using: :btree
    t.index ["email"], name: "index_invitations_on_email", using: :btree
    t.index ["invitation_token"], name: "index_invitations_on_invitation_token", using: :btree
    t.index ["sent_by_id"], name: "index_invitations_on_sent_by_id", using: :btree
    t.index ["status_entity_id"], name: "index_invitations_on_status_entity_id", using: :btree
  end

  create_table "organizations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 129, default: "", null: false
    t.integer  "status_entity_id",                          null: false
    t.string   "name",             limit: 100, default: "", null: false
    t.string   "website",          limit: 100, default: "", null: false
    t.string   "phone_number"
    t.string   "address"
    t.datetime "deleted_at"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["deleted_at"], name: "index_organizations_on_deleted_at", using: :btree
    t.index ["status_entity_id"], name: "index_organizations_on_status_entity_id", using: :btree
  end

  create_table "profile_pictures", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["user_id"], name: "index_profile_pictures_on_user_id", using: :btree
  end

  create_table "questions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",                limit: 129,   default: "", null: false
    t.text     "content",             limit: 65535,              null: false
    t.integer  "score"
    t.boolean  "multiple_answers"
    t.boolean  "random_answer_order"
    t.integer  "scorecard_id"
    t.integer  "type_entity_id"
    t.datetime "deleted_at"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.index ["deleted_at"], name: "index_questions_on_deleted_at", using: :btree
    t.index ["scorecard_id"], name: "index_questions_on_scorecard_id", using: :btree
    t.index ["type_entity_id"], name: "index_questions_on_type_entity_id", using: :btree
  end

  create_table "requests_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 129, default: "", null: false
    t.string   "name",             limit: 250,              null: false
    t.boolean  "scorecard"
    t.integer  "organization_id",                           null: false
    t.integer  "status_entity_id",                          null: false
    t.integer  "type_entity_id",                            null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["deleted_at"], name: "index_requests_categories_on_deleted_at", using: :btree
    t.index ["organization_id"], name: "index_requests_categories_on_organization_id", using: :btree
    t.index ["status_entity_id"], name: "index_requests_categories_on_status_entity_id", using: :btree
    t.index ["type_entity_id"], name: "index_requests_categories_on_type_entity_id", using: :btree
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "uuid"
    t.string   "name"
    t.string   "sort_name"
    t.string   "class_name"
    t.string   "description"
    t.integer  "status_entity_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_roles_on_deleted_at", using: :btree
    t.index ["status_entity_id"], name: "index_roles_on_status_entity_id", using: :btree
  end

  create_table "scorecards", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",                     limit: 129, default: "", null: false
    t.boolean  "gradable"
    t.integer  "pass_grade"
    t.boolean  "different_question_order"
    t.boolean  "type_different_question"
    t.boolean  "random_questions_order"
    t.integer  "num_questions"
    t.integer  "requests_category_id",                              null: false
    t.integer  "status_entity_id",                                  null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.index ["deleted_at"], name: "index_scorecards_on_deleted_at", using: :btree
    t.index ["requests_category_id"], name: "index_scorecards_on_requests_category_id", using: :btree
    t.index ["status_entity_id"], name: "index_scorecards_on_status_entity_id", using: :btree
  end

  create_table "states", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 129,   default: "", null: false
    t.integer  "status_entity_id",                            null: false
    t.integer  "country_id",                                  null: false
    t.integer  "user_id",                                     null: false
    t.string   "sort_name",        limit: 6,     default: "", null: false
    t.string   "name",             limit: 250,   default: "", null: false
    t.text     "description",      limit: 65535
    t.string   "time_zone"
    t.datetime "deleted_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.index ["country_id"], name: "index_states_country_id", using: :btree
    t.index ["country_id"], name: "index_states_on_country_id", using: :btree
    t.index ["deleted_at"], name: "index_states_on_deleted_at", unique: true, using: :btree
    t.index ["status_entity_id"], name: "index_states_on_status_entity_id", using: :btree
    t.index ["status_entity_id"], name: "index_states_status_entity_id", using: :btree
    t.index ["user_id"], name: "index_states_on_user_id", using: :btree
  end

  create_table "status_entities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",            limit: 129, default: "",  null: false
    t.string   "sort_name",       limit: 6,                 null: false
    t.string   "name",            limit: 100,               null: false
    t.string   "class_name",      limit: 100, default: "",  null: false
    t.boolean  "status"
    t.string   "created_by_uuid",             default: "1", null: false
    t.string   "updated_by_uuid",             default: "1", null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["class_name"], name: "index_status_entities_on_class_name", using: :btree
    t.index ["deleted_at"], name: "index_status_entities_on_deleted_at", using: :btree
  end

  create_table "type_entities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",             limit: 129,   default: "", null: false
    t.integer  "status_entity_id",                            null: false
    t.string   "sort_name",        limit: 6,                  null: false
    t.string   "class_name",       limit: 100,   default: "", null: false
    t.string   "name",             limit: 100,   default: "", null: false
    t.text     "description",      limit: 65535
    t.string   "created_by_uuid",  limit: 100,   default: "", null: false
    t.string   "updated_by_uuid",  limit: 100,   default: "", null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.index ["deleted_at"], name: "index_type_entities_on_deleted_at", using: :btree
    t.index ["status_entity_id"], name: "index_type_entities_on_status_entity_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",                   limit: 129, default: "",   null: false
    t.integer  "status_entity_id",                                  null: false
    t.integer  "organization_id"
    t.string   "email",                              default: "",   null: false
    t.string   "encrypted_password",                 default: "",   null: false
    t.string   "first_name",             limit: 100, default: "",   null: false
    t.string   "last_name",              limit: 100, default: "",   null: false
    t.string   "middle_name",            limit: 100, default: ""
    t.string   "second_last_name",       limit: 100, default: ""
    t.string   "full_name",                          default: ""
    t.string   "organization_name",                  default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.boolean  "status",                             default: true
    t.string   "created_by_uuid",                    default: "1",  null: false
    t.string   "updated_by_uuid",                    default: "1",  null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.string   "invited_by_type"
    t.integer  "invited_by_id"
    t.integer  "invitations_count",                  default: 0
    t.string   "avatar"
    t.integer  "profile_picture"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["deleted_at"], name: "index_users_on_deleted_at", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
    t.index ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
    t.index ["organization_id"], name: "index_users_on_organization_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["status_entity_id"], name: "index_users_on_status_entity_id", using: :btree
  end

  create_table "users_organizations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",                  limit: 129, default: "", null: false
    t.integer  "user_id",                                        null: false
    t.integer  "organization_id",                                null: false
    t.integer  "users_organization_id"
    t.string   "job_title",             limit: 250, default: ""
    t.integer  "status_entity_id"
    t.datetime "deleted_at"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.index ["deleted_at"], name: "index_users_organizations_on_deleted_at", using: :btree
    t.index ["organization_id"], name: "index_users_organizations_on_organization_id", using: :btree
    t.index ["status_entity_id"], name: "index_users_organizations_on_status_entity_id", using: :btree
    t.index ["user_id"], name: "index_users_organizations_on_user_id", using: :btree
    t.index ["users_organization_id"], name: "index_users_organizations_on_users_organization_id", using: :btree
  end

  create_table "users_organizations_roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uuid",                  limit: 129, default: "", null: false
    t.integer  "users_organization_id",                          null: false
    t.integer  "role_id",                                        null: false
    t.integer  "status_entity_id",                               null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.index ["deleted_at"], name: "index_users_organizations_roles_on_deleted_at", using: :btree
    t.index ["role_id"], name: "index_users_organizations_roles_on_role_id", using: :btree
    t.index ["status_entity_id"], name: "index_users_organizations_roles_on_status_entity_id", using: :btree
    t.index ["users_organization_id"], name: "index_users_organizations_roles_on_users_organization_id", using: :btree
  end

  add_foreign_key "answers", "questions"
  add_foreign_key "cities", "states"
  add_foreign_key "cities", "status_entities"
  add_foreign_key "cities", "users"
  add_foreign_key "counties", "states"
  add_foreign_key "counties", "status_entities"
  add_foreign_key "counties", "users"
  add_foreign_key "countries", "status_entities"
  add_foreign_key "countries", "users"
  add_foreign_key "countries_currencies", "countries"
  add_foreign_key "countries_currencies", "currencies"
  add_foreign_key "currencies", "status_entities"
  add_foreign_key "invitations", "status_entities"
  add_foreign_key "invitations", "users_organizations", column: "sent_by_id"
  add_foreign_key "organizations", "status_entities"
  add_foreign_key "profile_pictures", "users"
  add_foreign_key "questions", "scorecards"
  add_foreign_key "questions", "type_entities"
  add_foreign_key "requests_categories", "organizations"
  add_foreign_key "requests_categories", "status_entities"
  add_foreign_key "requests_categories", "type_entities"
  add_foreign_key "roles", "status_entities"
  add_foreign_key "scorecards", "requests_categories"
  add_foreign_key "scorecards", "status_entities"
  add_foreign_key "states", "countries"
  add_foreign_key "states", "status_entities"
  add_foreign_key "states", "users"
  add_foreign_key "type_entities", "status_entities"
  add_foreign_key "users", "organizations"
  add_foreign_key "users", "status_entities"
  add_foreign_key "users_organizations", "organizations"
  add_foreign_key "users_organizations", "status_entities"
  add_foreign_key "users_organizations", "users"
  add_foreign_key "users_organizations", "users_organizations"
  add_foreign_key "users_organizations_roles", "roles"
  add_foreign_key "users_organizations_roles", "status_entities"
  add_foreign_key "users_organizations_roles", "users_organizations"
end
