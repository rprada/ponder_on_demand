class CreateUsersOrganizationsRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :users_organizations_roles do |t|
      t.string      :uuid,                null: false,  limit: 129,  default: ""
      t.references  :users_organization,  null: false, foreign_key: true
      t.references  :role,                null: false, foreign_key: true
      t.references  :status_entity,       null: false, foreign_key: true
      t.datetime    :deleted_at, index: true, unique: true

      t.timestamps
    end
  end
end