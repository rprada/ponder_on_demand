class CreateCoreTypeEntities < ActiveRecord::Migration[5.0]
  def change
    create_table :type_entities do |t|
      t.string      :uuid,        null: false,  limit: 129, default: ""
      t.references  :status_entity,       null: false, foreign_key: true
      t.string      :sort_name,           null: false,  limit: 6,    deault: 'ACT'
      t.string      :class_name,          null: false,  limit: 100,  default: ''
      t.string      :name,                null: false,  limit: 100,  default: ''
      t.text        :description,         null: true
      t.string      :created_by_uuid,     null: false,  limit: 100,  default: ''
      t.string      :updated_by_uuid,     null: false,  limit: 100,  default: ''
      t.datetime    :deleted_at, index: true, unique: true
      t.timestamps   null: false
    end
  end
end