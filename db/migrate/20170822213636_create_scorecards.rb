class CreateScorecards < ActiveRecord::Migration[5.0]
  def change
    create_table :scorecards do |t|
      t.string     :uuid,                       null: false,  limit: 129, default: ""
      t.boolean    :gradable
      t.integer    :pass_grade,                 null: true
      t.boolean    :different_question_order
      t.boolean    :type_different_question
      t.boolean    :random_questions_order
      t.integer    :num_questions,              null: true
      t.references :requests_category,          null: false, foreign_key: true
      t.references :status_entity,              null: false, foreign_key: true , index: true
      t.datetime :deleted_at,                   index: true, unique: true
      t.timestamps null: false
    end
  end
end