class AddStatusEntityToUsersOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_reference :users_organizations, :status_entity, index: true, foreign_key: true,  after: :organization_id
  end
end
