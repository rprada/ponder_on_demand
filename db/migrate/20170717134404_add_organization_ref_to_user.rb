class AddOrganizationRefToUser < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :organization, index: true, foreign_key: true, after: :status_entity_id
  end
end