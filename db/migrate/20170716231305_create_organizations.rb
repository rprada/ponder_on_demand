class CreateOrganizations < ActiveRecord::Migration[5.0]
  def change
    create_table :organizations do |t|
      t.string     :uuid,           null: false,  limit: 129,  default: ""
      t.references :status_entity,  null: false,  foreign_key: true
      t.string     :name,           null: false,  limit: 100,  default: ''
      t.string     :website,        null: false,  limit: 100,  default: ''
      t.string     :phone_number,   null: true 
      t.string     :address,        null: true   
      t.datetime   :deleted_at, index: true, unique: true
      t.timestamps null: false
    end
  end
end