class CreateStates < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.string      :uuid,           null: false,  limit: 129, default: ""
      t.references  :status_entity,  null: false,  foreign_key: true
      t.references  :country,        null: false,  foreign_key: true
      t.references  :user,           null: false,  foreign_key: true
      t.string      :sort_name,      null: false,  limit: 6,   default: ''
      t.string      :name,           null: false,  limit: 250, default: ''
      t.text        :description
      t.string      :time_zone,      null: true
      t.datetime :deleted_at
      t.timestamps null: false
    end
     add_index :states,  :deleted_at, unique: true
     add_index :states,  :country_id, name: "index_states_country_id"
     add_index :states, :status_entity_id, name: "index_states_status_entity_id"
   end
end
