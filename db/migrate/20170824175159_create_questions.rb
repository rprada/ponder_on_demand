class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.string      :uuid,                   null: false,  limit: 129, default: ""
      t.text        :content,                null: false
      t.integer     :score,                  null: true
      t.boolean     :multiple_answers
      t.boolean     :random_answer_order
      t.references  :scorecard,             foreign_key: true, index: true
      t.references  :type_entity,           foreign_key: true, index: true
      t.datetime    :deleted_at,            index: true, unique: true      
      t.timestamps  null: false
    end
  end
end