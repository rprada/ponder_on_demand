class CreateUsersOrganizations < ActiveRecord::Migration[5.0]
  def change
    create_table :users_organizations do |t|
      t.string     :uuid,          null: false,  limit: 129, default: ""
      t.references :user,          null: false, foreign_key: true
      t.references :organization,  null: false, foreign_key: true
      t.datetime   :deleted_at, index: true, unique: true
      t.timestamps null: false
    end
  end
end
