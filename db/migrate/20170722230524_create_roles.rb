class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.integer :uuid
      t.string :name
      t.string :sort_name
      t.string :class_name
      t.string :description
      t.references :status_entity, foreign_key: true

      t.timestamps
    end
  end
end
