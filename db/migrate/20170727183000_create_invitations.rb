class CreateInvitations < ActiveRecord::Migration[5.0]
  def change
    create_table :invitations do |t|
      t.string     :uuid,               null: false,  limit: 129, default: ""
      t.integer    :sent_by_id,         null: false
      t.string     :email,              null: false
      #t.references :users_organization, null: false,  foreign_key: true
      t.string     :invitation_token, null: false 
      t.references :status_entity,  null: false,  foreign_key: true
      
      t.datetime   :deleted_at, index: true, unique: true
      t.timestamps null: false
    end

    add_index :invitations, :sent_by_id
    add_index :invitations, :email
    add_index :invitations, :invitation_token

    add_foreign_key :invitations, :users_organizations, column: :sent_by_id
  end
end
