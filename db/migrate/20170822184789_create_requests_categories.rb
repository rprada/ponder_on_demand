class CreateRequestsCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :requests_categories do |t|
      t.string     :uuid,                   null: false,  limit: 129, default: ""
      t.string     :name,                   null: false,  limit: 250
      t.boolean    :scorecard
      t.references :organization,           null: false, foreign_key: true, index: true
      t.references :status_entity,          null: false, foreign_key: true, index: true
      t.references :type_entity,            null: false, foreign_key: true, index: true
      t.datetime   :deleted_at,             index: true, unique: true
      t.timestamps
    end
  end
end