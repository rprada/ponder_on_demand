class AddManageByAndJobTitleToUsersOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_reference :users_organizations, :users_organization, index: true, foreign_key: true,  after: :organization_id
    add_column :users_organizations, :job_title, :string, null: true,  limit: 250, default: '', after: :users_organization_id
  end
end
