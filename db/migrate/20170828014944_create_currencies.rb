class CreateCurrencies < ActiveRecord::Migration[5.0]
  def change
    create_table :currencies do |t|
      t.string      :uuid,           null: false,  limit: 200, default: ""
      t.references  :status_entity,  null: false,  foreign_key: true, index: true
      t.string      :sort_name,      null: false,  limit: 10 ,   default: ''
      t.string      :name,           null: false,  limit: 250,  default: ''
      t.string      :symbol,         null: false,  limit: 250,  default: ''
      t.datetime    :deleted_at , index: true, unique: true
      t.timestamps null: false
    end
  end
end
