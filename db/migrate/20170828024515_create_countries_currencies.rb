class CreateCountriesCurrencies < ActiveRecord::Migration[5.0]
  def change
    create_table :countries_currencies do |t|
      t.references :country,      null: false, foreign_key: true
      t.references :currency,     null: false, foreign_key: true
      t.datetime   :deleted_at,   index: true, unique: true
      t.timestamps null: false    end
  end
end
