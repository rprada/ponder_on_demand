class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
      t.string     :uuid,           null: false,  limit: 129, default: ""
      t.references :status_entity,  null: false,  foreign_key: true,  index: true
      t.references :user,           null: false,  foreign_key: true,  index: true
      t.string     :sort_name,      null: false,  limit: 6,   deault: ''
      t.string     :name,           null: false,  limit: 250, default: ''
      t.string     :language,       null: false,  limit: 100, default: ''
      t.text       :description
      t.datetime :deleted_at
      t.timestamps null: false
    end
    add_index :countries, :deleted_at,           unique: true
  end
end
