class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
      t.string      :uuid,           null: false,  limit: 200, default: ""
      t.references  :status_entity,  null: false,  foreign_key: true, index: true
      t.references  :state,          null: true,   foreign_key: true, index: true
      t.references  :user,           null: false,  foreign_key: true, index: true
      t.integer     :county_id,      null: true
      t.string      :sort_name,      null: false,  limit: 6 ,   default: ''
      t.string      :name,           null: false,  limit: 250,  default: ''
      t.text        :description
      t.datetime :deleted_at , index: true, unique: true
      t.timestamps null: false
    end
  end
end
