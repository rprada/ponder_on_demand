class CreateCoreStatusEntities < ActiveRecord::Migration[5.0]
  def change
    create_table :status_entities do |t|
      t.string     :uuid,        null: false,  limit: 129, default: ""
      t.string     :sort_name,   null: false,  limit: 6,   deault: 'ACT'
      t.string     :name,        null: false,  limit: 100,   deault: 'ACT'
      t.string     :class_name,  null: false,  limit: 100, default: ''
      t.boolean    :status,      defaul: true
      t.string     :created_by_uuid,  default: 1, null: false
      t.string     :updated_by_uuid,  default: 1, null: false
      t.datetime   :deleted_at, unique: true , index: true
      t.timestamps null: false
    end
    add_index :status_entities,  :class_name
  end
end
