class CreateAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :answers do |t|
      t.string     :uuid,              null: false,  limit: 129, default: ""
      t.text       :content,           null: false
      t.integer    :score,             null: true
      t.boolean    :correct_answer     
      t.references :question,          foreign_key: true, index:true
      t.datetime   :deleted_at,        index: true, unique: true      
      t.timestamps null: false
      t.timestamps
    end
  end
end