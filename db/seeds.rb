# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

@status_entity = StatusEntity.where(class_name: 'user', sort_name: 'Active')
if @status_entity.empty?
  Users = Array.new
  Organizations = Array.new
  Roles = Array.new
  UsersOrganizations = Array.new
  i = 0
  ii = 0
  user_uuid = SecureRandom.uuid
  status_entities = [
                      ['ACTV', 'Active', 'user'],
                      ['INACTV', 'Inactive', 'user'],
                      ['ACTV', 'Active', 'organization'],
                      ['INACTV', 'Inactive', 'organization'],
                      ['ACTV', 'Active', 'type_entity'],
                      ['INACTV', 'Inactive', 'type_entity'],
                      ['Accepted', 'ACPT', 'Invitation'],
                      ['Pending' , 'PEND', 'Invitation'],
                      ['Rejected', 'REJ' , 'Invitation'],
                      ['ACTV', 'Active', 'role'],
                      ['INACTV', 'Inactive', 'role'],
                      ['ACTV', 'Active', 'users_organization'],
                      ['INACTV', 'Inactive', 'users_organization'],
                      ['ACTV', 'Active', 'users_organizations_role'],
                      ['INACTV', 'Inactive', 'users_organizations_role'],
                      ['ACTV', 'Active', 'country'],
                      ['INACTV', 'Inactive', 'country'],
                      ['ACTV', 'Active', 'state'],
                      ['INACTV', 'Inactive', 'state'],
                      ['ACTV', 'Active', 'county'],
                      ['INACTV', 'Inactive', 'county'],
                      ['ACTV', 'Active', 'city'],
                      ['INACTV', 'Inactive', 'city']                      
                    ]
  status_entities.each do |sort_name, name, class_name|
    StatusEntity.create(sort_name:  sort_name,   name: name,   class_name: class_name,
                        uuid: SecureRandom.uuid,    created_at: Time.now, status: true,
                        created_by_uuid: user_uuid, updated_by_uuid: user_uuid)
  end

  status_entity = StatusEntity.where(class_name:'user', name: 'Active')
  users = [
            [ 'admin@ixanaui.com', SecureRandom.uuid.to_s, 'Admin' , 'Ixanaui' , 'Ixanaui' ],
          ]

  #status_entity = StatusEntity.actived('User')
  status_entity = StatusEntity.where(class_name:'user', name: 'Active')
  users.each do |email, uuid, first_name, last_name, organization_name |
    Users << User.create(email: email,  uuid: SecureRandom.uuid.to_s, first_name: first_name ,
                        last_name: last_name, password: '123qweasdzxc', status: true,
                        created_by_uuid: user_uuid,  updated_by_uuid: user_uuid , confirmed_at: DateTime.now,
                        status_entity_id: StatusEntity.first.id, organization_name: organization_name )
  end
  organizations = [
            [ 'ixanaui', SecureRandom.uuid.to_s, 'wwww.ixanaui.com' , '(555)555 555' , 'Mexico' ],
          ]
  organizations.each do |name, uuid, website, phone_number, address |
    Organizations << Organization.create(name: name,  uuid: SecureRandom.uuid.to_s, website: website ,
                        phone_number: phone_number, address: address,
                        status_entity_id: StatusEntity.find(3).id )
  end

  roles = [
            ['Administrator', 'Admin', 'Role'],
            ['Member Owner' , 'MOW'  , 'Role'],
            ['Member'       , 'MEB'  , 'Role'],
          ]

  roles.each do |name, sort_name, class_name|
    Role.create(name: name, sort_name: sort_name, class_name: class_name,
                uuid: SecureRandom.uuid, status_entity_id: StatusEntity.where(class_name: "role").first.id)
  end

  Users.each do |a|
    b = Array.new
    Organizations.each do |x|
      b << x
    end  
     UsersOrganizations << UsersOrganization.create(user_id: a.id, organization_id: b[i].id, 
                                                    status_entity_id: StatusEntity.where(class_name: "users_organization").first.id)
    i += 1
  end


  UsersOrganizations.each do |a|
    b = Array.new
    Roles.each do |x|
      b << x
    end  
    UsersOrganizationsRole.create(users_organization_id: a.id, role_id: b[i].id, 
                                  status_entity_id: StatusEntity.where(class_name: "users_organizations_role").first.id)
    ii += 1
  end

end