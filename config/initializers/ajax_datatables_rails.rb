AjaxDatatablesRails.configure do |config|
  # available options for db_adapter are: :pg, :mysql, :mysql2, :sqlite, :sqlite3
   config.db_adapter = :mysql2

  # available options for paginator are: :simple_paginator, :kaminari, :will_paginate
  # config.paginator = :will_paginate
  # available options for orm are: :active_record, :mongoid
   config.orm = :active_record
   
end
