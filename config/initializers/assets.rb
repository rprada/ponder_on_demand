# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
  Rails.application.config.assets.precompile += %w( script.js )
  Rails.application.config.assets.precompile += %w( styles-core.css )
  Rails.application.config.assets.precompile += %w( required/circloid-functions.js )

  Rails.application.config.assets.precompile += %w( users/sessions.js )
  Rails.application.config.assets.precompile += %w( users/registrations.js )
  Rails.application.config.assets.precompile += %w( welcome/welcome.js )
  Rails.application.config.assets.precompile += %w( users/invitations.js )

  Rails.application.config.assets.precompile += %w( invitations.js )

  Rails.application.config.assets.precompile += %w( organizations.js )
  Rails.application.config.assets.precompile += %w( status_entities.js )
  Rails.application.config.assets.precompile += %w( type_entities.js )
  Rails.application.config.assets.precompile += %w( roles.js )
  Rails.application.config.assets.precompile += %w( users.js )
  Rails.application.config.assets.precompile += %w( users_organizations.js )
  Rails.application.config.assets.precompile += %w( countries.js )
  Rails.application.config.assets.precompile += %w( states.js )
  Rails.application.config.assets.precompile += %w( counties.js )
  Rails.application.config.assets.precompile += %w( cities.js )
  Rails.application.config.assets.precompile += %w( requests_categories.js )
  Rails.application.config.assets.precompile += %w( scorecards.js )
  Rails.application.config.assets.precompile += %w( currencies.js )

  Rails.application.config.assets.precompile += %w( dndTree.js )
  Rails.application.config.assets.precompile += %w( d3.v3.min.js )

  # Rails.application.config.assets.precompile += %w( modals.js )

  Rails.application.config.assets.precompile += %w( rails.validations.js )


#%W( users ).each do |controller|
#  Rails.application.config.assets.precompile += ["#{controller}.js"]
#end
