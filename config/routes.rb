Rails.application.routes.draw do

  resources :answers
  scope '(:locale)', constraints: { locale: /en|es/ } do
    
    devise_for :users,
     controllers: {
      sessions: 'users/sessions',
      passwords: 'users/passwords',
      registrations: 'users/registrations',
      confirmations: 'users/confirmations',
      invitations: 'users/invitations'
    }
    devise_scope :user do

      get 'invitation/accept',
        to: 'users/invitations#accept',
        as: :accept_invitation_request

      get 'users/invitations',
        to: 'users/invitations#index',
        as: :index_invitations

      get 'invitation/resend',
        to: 'users/invitations#resend',
        as: :resend_invitation

      get 'invitation/remove',
        to: 'users/invitations#remove',
        as: :remove_invitation

      authenticated :user do
        root :to => 'welcome#index', as: :authenticated_root
      end
      unauthenticated :user do
        root :to => 'users/sessions#new', as: :unauthenticated_root
      end
      get '/profile/(:id)', :action => 'profile', :controller => 'users/registrations', as: 'profile'
      # DEV - Experimentacion con routes, sin exito
      ########OJO CON ESTA RUTA#####
      get 'users/invitations/(:id)', :action => 'index', :controller => 'users/invitations', as: 'invitations'
      # namespace :users do
      # end
      # resources :users, controller: 'users/invitations'
      # get 'users/invitations/(:id)/new', :action => 'new', :controller => 'users/invitations', as: 'invitations/new'
        # accept_user_invitation GET    /users/invitation/accept(.:format)           users/invitations#edit
        # accept_invitations GEST    /invitations/accept(.:format)     invitations#accept

      ########OJO CON ESTA RUTA#####

      #get '/invitations/(:id)', :action => 'index', :controller => 'users/invitations', as: 'invitations'

      put 'users/(:id)', to: 'users/registrations#update'
      post 'checking/email/', to: 'users/registrations#checkEmail'
    end


    resources :organizations
    get 'organizations_datatable_index', to: 'organizations#datatable_index'
    resources :status_entities
    get 'status_entities_datatable_index', to: 'status_entities#datatable_index'
    resources :type_entities
    get 'type_entities_datatable_index', to: 'type_entities#datatable_index'
    resources :roles
    get 'roles_datatable_index', to: 'roles#datatable_index'
    resources :users
    get 'users_datatable_index', to: 'users#datatable_index'

    resources :users_organizations do
      get 'data_json'
      get 'org_tree'
    end

    resources :scorecards do
      get 'questionType'
    end
    put  'scorecards/:id/:id2'                =>    'scorecards#onlyQuestionsAnswers'
    put  'scorecards/question/:id/:id2'       =>    'scorecards#removeQuestionsAnswers'
    resources :questions do

    end
    resources :requests_categories do
      resources :scorecards do
        resources :questions do
        end
        get 'onlyQuestionsAnswers'
      end
      get 'scorecard_association'
      #get 'index'
    end
    get 'requests_categories_datatable_index', to: 'requests_categories#datatable_index'
    resources :users_organizations_roles

    get 'users_organizations_datatable_index', to: 'users_organizations#datatable_index'

    resources :countries
    get 'countries_datatable_index', to: 'countries#datatable_index'
    resources :states do
      get 'get_counties_by_state'
    end
    get 'states_datatable_index', to: 'states#datatable_index'
    resources :counties
    get 'counties_datatable_index', to: 'counties#datatable_index'
    resources :cities
    get 'cities_datatable_index', to: 'cities#datatable_index'

    resources :currencies do

    end
    get 'currencies_datatable_index'                =>    'currencies#datatable_index'


  end
end