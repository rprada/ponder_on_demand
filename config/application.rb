require_relative 'boot'

require 'rails/all'


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module PonderOnDemand
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    #Add this line to enable access to nested dictorinaries in locale
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true

    config.assets.paths << Rails.root.join("vendor","assets", "fonts")


 #   config.action_controller.asset_host = 'localhost:3000'
 #   config.action_mailer.asset_host = 'http://localhost:3000'

    config.action_mailer.smtp_settings = {
      :address              => "smtp.gmail.com",
      :port                 =>  587, # 25,
      :domain               => 'gmail.com',
      :user_name            => 'R.Alejandro989@gmail.com', # 'sistemas',
      :password             => 'Admin989rp.*',
      :authentication       => 'plain',
      #:ssl => true,
      :enable_starttls_auto => true, # Si lo colocaba en true daba este error= OpenSSL::SSL::SSLError (hostname does not match the server certificate)
	   #      :openssl_verify_mode  => OpenSSL::SSL::VERIFY_CLIENT_ONCE,
    }
  end
end
