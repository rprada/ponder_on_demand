require 'test_helper'

class UsersOrganizationsRolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_organizations_role = users_organizations_roles(:one)
  end

  test "should get index" do
    get users_organizations_roles_url
    assert_response :success
  end

  test "should get new" do
    get new_users_organizations_role_url
    assert_response :success
  end

  test "should create users_organizations_role" do
    assert_difference('UsersOrganizationsRole.count') do
      post users_organizations_roles_url, params: { users_organizations_role: { role_id: @users_organizations_role.role_id, users_organization: @users_organizations_role.users_organization } }
    end

    assert_redirected_to users_organizations_role_url(UsersOrganizationsRole.last)
  end

  test "should show users_organizations_role" do
    get users_organizations_role_url(@users_organizations_role)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_organizations_role_url(@users_organizations_role)
    assert_response :success
  end

  test "should update users_organizations_role" do
    patch users_organizations_role_url(@users_organizations_role), params: { users_organizations_role: { role_id: @users_organizations_role.role_id, users_organization: @users_organizations_role.users_organization } }
    assert_redirected_to users_organizations_role_url(@users_organizations_role)
  end

  test "should destroy users_organizations_role" do
    assert_difference('UsersOrganizationsRole.count', -1) do
      delete users_organizations_role_url(@users_organizations_role)
    end

    assert_redirected_to users_organizations_roles_url
  end
end
