require 'test_helper'

class RequestsCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @requests_category = requests_categories(:one)
  end

  test "should get index" do
    get requests_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_requests_category_url
    assert_response :success
  end

  test "should create requests_category" do
    assert_difference('RequestsCategory.count') do
      post requests_categories_url, params: { requests_category: { status_entity_id: @requests_category.status_entity_id } }
    end

    assert_redirected_to requests_category_url(RequestsCategory.last)
  end

  test "should show requests_category" do
    get requests_category_url(@requests_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_requests_category_url(@requests_category)
    assert_response :success
  end

  test "should update requests_category" do
    patch requests_category_url(@requests_category), params: { requests_category: { status_entity_id: @requests_category.status_entity_id } }
    assert_redirected_to requests_category_url(@requests_category)
  end

  test "should destroy requests_category" do
    assert_difference('RequestsCategory.count', -1) do
      delete requests_category_url(@requests_category)
    end

    assert_redirected_to requests_categories_url
  end
end
