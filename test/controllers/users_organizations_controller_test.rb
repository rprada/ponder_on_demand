require 'test_helper'

class UsersOrganizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_organization = users_organizations(:one)
  end

  test "should get index" do
    get users_organizations_url
    assert_response :success
  end

  test "should get new" do
    get new_users_organization_url
    assert_response :success
  end

  test "should create users_organization" do
    assert_difference('UsersOrganization.count') do
      post users_organizations_url, params: { users_organization: {  } }
    end

    assert_redirected_to users_organization_url(UsersOrganization.last)
  end

  test "should show users_organization" do
    get users_organization_url(@users_organization)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_organization_url(@users_organization)
    assert_response :success
  end

  test "should update users_organization" do
    patch users_organization_url(@users_organization), params: { users_organization: {  } }
    assert_redirected_to users_organization_url(@users_organization)
  end

  test "should destroy users_organization" do
    assert_difference('UsersOrganization.count', -1) do
      delete users_organization_url(@users_organization)
    end

    assert_redirected_to users_organizations_url
  end
end
